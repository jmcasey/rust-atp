#![allow(dead_code)]

pub mod did;
pub mod lexicon;
pub mod repo;

// Sample Data
const PDS_XRPC_URL: &str = "http://localhost:2583/xrpc/";
const COLLECTION: &str = "app.bsky.feed.post";
