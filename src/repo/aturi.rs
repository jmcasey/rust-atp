use regex::Regex;

#[derive(Clone, Debug)]
pub struct AtUri {
    pub protocol: Option<String>,
    pub origin: Option<String>,
    pub hostname: Option<String>,
    pub collection: Option<String>,
    pub rkey: Option<String>,
    pub fragment: Option<String>,
}

pub fn some_string_from_match(key: &str, caps: &regex::Captures) -> Option<String> {
    caps.name(key).map(|value| String::from(value.as_str()))
}

//
// Current definition:
// https://github.com/bluesky-social/atproto/tree/main/packages/uri
//
// Example URI:
// at://did:plc:uqp3gtok3lce3vmcogixidnb/app.bsky.feed.post/3jgwwh3r4dk2p
//
// If parsing is successful, returns an instance of AtUri will all successfully
// parsed elements. Otherwise returns none. Propogates errors from regex.
//

impl AtUri {
    pub fn from(url: &str) -> Result<Option<AtUri>, regex::Error> {
        let re = Regex::new(
            r"(?x)
            ^(?P<origin>(?P<protocol>at://)(?P<hostname>.{32}))
            /?
            (?P<collection>[^/]+)
            /?
            (?P<rkey>.+)
            #?
            (?P<fragment>.?)??$      
        ",
        )?;

        let ret: Option<AtUri> = re.captures(url).map(|caps| AtUri {
            protocol: some_string_from_match("protocol", &caps),
            origin: some_string_from_match("origin", &caps),
            hostname: some_string_from_match("hostname", &caps),
            collection: some_string_from_match("collection", &caps),
            rkey: some_string_from_match("rkey", &caps),
            fragment: some_string_from_match("fragment", &caps),
        });

        Ok(ret)
    }
}
