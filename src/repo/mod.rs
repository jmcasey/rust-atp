use serde::{Deserialize, Serialize};

pub mod aturi;
pub mod mst;
pub mod mst_node;
pub mod tid;

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Value {
    pub text: String,
    #[serde(alias = "$type")]
    pub r#type: String,
    pub created_at: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Record {
    pub uri: String,
    pub cid: String,
    pub value: Value, // see Value above
}
