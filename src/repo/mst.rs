use libipld::{DagCbor, Ipld};
use libipld_macro::ipld;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

#[derive(Clone, DagCbor, Debug, Deserialize, Serialize)]
pub struct Commit {
    // Map of root and sig Ipld objects
    pub root: String,
    pub sig: Vec<u8>,
}

impl Commit {
    pub async fn new(root: String, sig: &[u8]) -> Ipld {
        // A commit is a map of two Ipld objects
        let mut map = BTreeMap::new();
        map.insert(String::from("root"), ipld!(root));
        map.insert(String::from("sig"), ipld!(sig));
        ipld!(map)
    }
}

#[derive(Clone, Debug, DagCbor, Deserialize, Serialize)]
pub struct RootNode {
    pub meta: String,         // TODO: DID object
    pub prev: Option<String>, // Link to CID of prev RootNode
    pub data: String,         // Link to CID of topmost MstNode
    pub auth_token: Option<String>, // The jwt-encoded UCAN (user controlled
                              // auth network) that gives authority to
                              // make the write which produced this root
}

impl RootNode {
    pub fn new(
        meta: String,
        prev: Option<String>,
        data: String,
        auth_token: Option<String>,
    ) -> Ipld {
        let prev = match prev {
            Some(cid) => ipld!(cid),
            None => ipld!(null), // Rust doesn't have nulls, but CBOR does.
        };

        let auth_token = match auth_token {
            Some(cid) => ipld!(cid),
            None => ipld!(null),
        };

        let mut map = BTreeMap::new();
        map.insert(String::from("meta"), ipld!(meta));
        map.insert(String::from("prev"), prev);
        map.insert(String::from("data"), ipld!(data));
        map.insert(String::from("auth_token"), auth_token);
        ipld!(map)
    }
}

#[derive(Clone, Debug, DagCbor, Deserialize, Serialize)]
pub struct RootMeta {
    pub datastore: String,
    pub did: String, // TODO: DID object
    pub version: u8,
}

impl RootMeta {
    pub fn new(datastore: String, did: String, version: u8) -> Ipld {
        let mut map = BTreeMap::new();
        map.insert(String::from("datastore"), ipld!(datastore));
        map.insert(String::from("did"), ipld!(did));
        map.insert(String::from("version"), ipld!(version));
        ipld!(map)
    }
}

#[derive(Clone, Debug, DagCbor, Deserialize, Serialize)]
pub struct MstNode {
    pub l: Option<String>, // (Optional) The CID of the leftmost subtree
    pub e: Vec<MstEntry>,  // An array of MST Entries
}

impl MstNode {
    pub fn new(l: Option<String>, _e: Vec<String>) -> Ipld {
        let l = match l {
            Some(cid) => ipld!(cid),
            None => ipld!(null),
        };
        let mut map = BTreeMap::new();
        map.insert(String::from("l"), l);
        //map.insert(String::from("e"), ipld!(e)); le bug - need to handle arrays of Entries
        ipld!(map)
    }
}

#[derive(Clone, Debug, DagCbor, Deserialize, Serialize)]
pub struct MstEntry {
    pub p: u32,    // Prefix count of utf-8 chars that this key shares with the prev key
    pub k: String, // The rest of the key outside the shared prefix
    pub v: String, // The CID of the value of the entry
    pub t: Option<String>, // (Optional) The CID of the next subtree (to the right of the leaf)
}
