use super::mst::Commit;
use super::mst::MstEntry;
use super::mst::MstNode;
use super::mst::RootMeta;
use super::mst::RootNode;
use libipld::Ipld;

pub trait AtpMstNode {
    fn from_ipld(&self, ipld: Ipld) -> Result<Box<Self>, libipld::error::Error>
    where
        Self: serde::de::DeserializeOwned;

    fn next_node(&self) -> Option<String>;
}

impl AtpMstNode for Commit {
    fn from_ipld(&self, ipld: Ipld) -> Result<Box<Self>, libipld::error::Error> {
        libipld_core::serde::from_ipld(ipld).map_err(|e| e.into())
    }
    fn next_node(&self) -> Option<String> {
        Some(self.root.clone())
    }
}

impl AtpMstNode for RootNode {
    fn from_ipld(&self, ipld: Ipld) -> Result<Box<Self>, libipld::error::Error> {
        libipld_core::serde::from_ipld(ipld).map_err(|e| e.into())
    }
    fn next_node(&self) -> Option<String> {
        Some(self.data.clone())
    }
}

impl AtpMstNode for RootMeta {
    fn from_ipld(&self, ipld: Ipld) -> Result<Box<Self>, libipld::error::Error> {
        libipld_core::serde::from_ipld(ipld).map_err(|e| e.into())
    }
    fn next_node(&self) -> Option<String> {
        None
    }
}

impl AtpMstNode for MstNode {
    fn from_ipld(&self, ipld: Ipld) -> Result<Box<Self>, libipld::error::Error> {
        libipld_core::serde::from_ipld(ipld).map_err(|e| e.into())
    }
    fn next_node(&self) -> Option<String> {
        self.l.clone()
    }
}

impl AtpMstNode for MstEntry {
    fn from_ipld(&self, ipld: Ipld) -> Result<Box<Self>, libipld::error::Error> {
        libipld_core::serde::from_ipld(ipld).map_err(|e| e.into())
    }
    fn next_node(&self) -> Option<String> {
        self.t.clone()
    }
}
