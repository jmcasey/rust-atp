use chrono::{DateTime, Duration, SecondsFormat, Utc};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Tid(String); // DateTime?

impl Tid {
    pub fn format(self) -> String {
        let tid_str = self.encode();
        match tid_str.len() {
            13 => {
                format!(
                    "{}-{}-{}-{}",
                    &tid_str[0..4],
                    &tid_str[4..7],
                    &tid_str[7..11],
                    &tid_str[11..13],
                )
            }
            _ => "Invalid TID length".to_string(),
        }
    }

    //BASE32_NOPAD.encode(self.to_microseconds().unwrap().to_string().as_bytes())

    pub fn now() -> Self {
        Tid(Utc::now().to_rfc3339_opts(SecondsFormat::Micros, false))
    }

    // Timezones aren't real. They can't hurt you.
    pub fn from_datetime(datetime: chrono::DateTime<Utc>) -> Self {
        Tid(datetime.to_rfc3339())
    }

    pub fn to_datetime(self) -> DateTime<Utc> {
        match DateTime::parse_from_rfc3339(self.0.as_str()) {
            Ok(dt) => dt.with_timezone(&Utc),
            _ => unreachable!("Tid cannot be parsed"),
        }
    }

    pub fn from_microseconds(i: i64) -> Tid {
        let dt = DateTime::<Utc>::default() + Duration::microseconds(i);
        Tid::from_datetime(dt)
    }

    pub fn to_microseconds(self) -> i64 {
        self.to_datetime().timestamp_micros()
    }

    /// Sort order invariant base32 encoding
    ///
    /// let dt = chrono::DateTime::parse_from_rfc3339(
    ///     "2023-01-26T00:03:06.996975137+00:00"
    /// );
    ///
    /// let tid = Tid::from_datetime(dt);
    /// let ts = tid.encode();
    /// asserteq!(ts, "3jn5xwnpybj")    
    ///
    /// let tid = Tid::from_datetime(dt);
    /// let ts = tid.decode();
    /// asserteq!(ts, "3jn5xwnpybj")
    ///
    ///
    // TODO: This is not currently compatible with the reference implementation
    // https://github.com/bluesky-social/atproto/blob/287fc40f4896e56da7a01790bb91e7fab47d34a8/packages/common/src/tid.ts#L39
    pub fn encode(self) -> String {
        let s32_char = "234567abcdefghijklmnopqrstuvwxyz";
        let mut s = String::new();
        let mut i = self.to_microseconds();
        while i != 0 {
            let c = i % 32;
            i /= 32;
            s.insert(0, s32_char.chars().nth(c as usize).unwrap());
        }
        s
    }

    pub fn decode(s: &str) -> Option<Tid> {
        let s32_char = "234567abcdefghijklmnopqrstuvwxyz";

        let mut i: i64 = 0;
        for c in s.chars() {
            match s32_char.find(c) {
                Some(n) => i = i * 32 + n as i64,
                _ => break,
            }
        }

        Some(Tid::from_microseconds(i))
    }
}
