use bytes::Bytes;
use reqwest_wasm::Response;
use serde::Deserialize;
use jsonschema::{Draft, JSONSchema};

pub mod schema;

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Auth {
    pub did: String,
    pub handle: String,
    pub access_jwt: String,
    pub refresh_jwt: String,
}

#[derive(Debug)]
pub struct Session {
    pub client: reqwest::Client,
    pub xrpc_url: String,
    pub auth: Option<Auth>,
}

/// ```rust
/// # use std::error::Error;
/// # use skysail::xrpc::{
/// #     Auth,
/// #     Session,
/// # };
///
/// # fn main() -> Result<(), Box<dyn Error>> {
///
///    let mut session = Session::new("http://localhost:2583/xrpc/");
///
///    let input = serde_json::json!(
///        {
///            // https://atproto.com/lexicons/com-atproto-account
///            "handle": "jik.test",
///            "password": "password",
///        }
///    );
///
///    let session_creation_xrpc = session
///        .xrpc("com.atproto.session.create", "procedure") // returns Xrpc
///        .input(Some(input))
///        .params(None);
///
///    let auth_json = session.execute(session_creation_xrpc);
///
/// #     Ok(())
/// # }
/// ```

impl Session {
    pub fn new(xrpc_url: &str) -> Session {
        let xrpc = Session {
            client: reqwest::Client::new(),
            xrpc_url: xrpc_url.to_string(),
            auth: None,
        };
        return xrpc;
    }

    pub async fn query(
        &self,
        method: &str,
        parameters: Option<serde_json::Value>,
    ) -> Result<Response, reqwest::Error> {
        let request = self.client.get(format!("{}{}", self.xrpc_url, method));
        let request = match &parameters {
            Some(json) => request.query(json),
            None => request,
        };

        request.send().await
    }

    pub async fn procedure(
        &self,
        method: &str,
        parameters: Option<serde_json::Value>,
        input: Option<serde_json::Value>,
    ) -> Result<Response, reqwest::Error> {
        let request = self.client.post(format!("{}{}", self.xrpc_url, method));
        let request = match parameters {
            Some(json) => request.query(&json),
            None => request,
        };

        let request = match input {
            Some(input) => request.json(&input),
            None => request,
        };

        let request = match &self.auth {
            Some(auth) => request.header("Authorization", format!("Bearer {}", auth.access_jwt)),
            None => request,
        };

        request.send().await
    }

    pub fn xrpc(&self, method: &str /* NSID */, r#type: &str) -> Xrpc {
        // TODO: Lexicon integration

        Xrpc {
            method: method.to_string(),
            r#type: r#type.to_string(),
            xrpc_url: self.xrpc_url.clone(),
            auth: self.auth.clone(),
            input: None,
            params: None,
        }
    }

    pub async fn execute(&self, xrpc: Xrpc) -> Result<serde_json::Value, reqwest::Error> {
        // validate input







        let result = match xrpc.r#type.as_str() {
            "query" => self.query(&xrpc.method, xrpc.params).await,
            "procedure" => self.procedure(&xrpc.method, xrpc.params, xrpc.input).await,
            &_ => todo!(),
        };

        // TODO: Support non-JSON encodings
        // TODO: Lexicon integrations

        result?.json().await
    }

    // temporary, will generalise into execute()
    pub async fn execute_cbor(&self, xrpc: Xrpc) -> Result<Bytes, reqwest::Error> {
        let result = match xrpc.r#type.as_str() {
            "query" => self.query(&xrpc.method, xrpc.params).await,
            "procedure" => self.procedure(&xrpc.method, xrpc.params, xrpc.input).await,
            &_ => todo!(),
        };

        // TODO: Support non-JSON encodings
        // TODO: Lexicon integrations

        result?.bytes().await
    }

    pub async fn get_did_doc(plc_url: &str, did: &str) -> Result<String, reqwest::Error> {
        let resp = reqwest::get(format!("{}{}", plc_url, did)).await?;
        let did = resp.text().await?;
        Ok(did)
    }
}

#[derive(Clone, Debug)]
pub struct Xrpc {
    pub method: String, // NSID / struct Method?
    pub r#type: String,
    pub xrpc_url: String,
    pub auth: Option<Auth>,
    pub input: Option<serde_json::Value>,
    pub params: Option<serde_json::Value>,
}

impl Xrpc {
    pub fn input(mut self, json: Option<serde_json::Value>) -> Self {
        self.input = json;
        self
    }

    pub fn params(mut self, json: Option<serde_json::Value>) -> Self {
        self.params = json;
        self
    }


    pub fn r#type(mut self, r#type: String) -> Self {
        self.r#type = r#type;
        self
    }

    pub fn validate(&self) {
        let schema = JSONSchema::options()
            .with_draft(Draft::Draft7)
            .compile(&serde_json::json!(&schema::APP_BSKY_ACTOR_PROFILE))
            .expect("schema");

        if let Some(input_json) = &self.input {
            let result = schema.validate(input_json);
            if let Err(errors) = result {
                for error in errors {
                    println!("Validation error: {}", error);
                    println!("Instance path: {}", error.instance_path);
                }
            }
        }
    }
}
