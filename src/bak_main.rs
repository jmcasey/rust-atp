#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_variables)]

use codegen::Scope;
use convert_case::{Case, Casing};
use libipld::codec::Codec;
use libipld::Block;
use libipld::Cid;
use libipld::DefaultParams;
use libipld::Ipld;
use libipld_cbor::DagCborCodec;
use nsid::Nsid;
use rand::distributions::Alphanumeric;
use rand::prelude::*;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use skysail::lexicon::code_generator;
use skysail::lexicon::core::LexObject;
use skysail::lexicon::core::LexRecord;
use skysail::lexicon::core::LexToken;
use skysail::lexicon::core::LexUserType;
use skysail::lexicon::core::LexXrpcBody;
use skysail::lexicon::core::LexXrpcProcedure;
use skysail::lexicon::core::LexXrpcQuery;
use skysail::lexicon::core::LexiconDef;
use skysail::lexicon::core::RustType;
use skysail::lexicon::core::UserType;
use skysail::lexicon::primatives::LexDatetime;
use skysail::lexicon::primatives::LexPrimitive;
use skysail::lexicon::primatives::LexString;
use skysail::repo::mst::Commit;
use skysail::repo::mst::MstNode;
use skysail::repo::mst::RootNode;
use skysail::repo::mst_node::AtpMstNode;
use skysail::repo::tid::Tid;
use skysail::store::Store;
use skysail::xrpc::Session;
use std::error::Error;
use std::fs::read_dir;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::str::FromStr;

use iroh_car::CarReader;

pub mod did;
pub mod lexicon;
pub mod repo;
pub mod store;
pub mod xrpc;

// Sample Data
const EMAIL: &str = "jik@example.com";
const USERNAME: &str = "jik.test"; // As of 2022-11-07 only .test domain is supported.
const PASSWORD: &str = "password";
const PDS_XRPC_URL: &str = "http://localhost:2583/xrpc/";
const DID_PLC_URL: &str = "http://localhost:2582";
const COLLECTION: &str = "app.bsky.feed.post";

fn read_bytes_from_file(file_path: &str) -> Vec<u8> {
    let mut file = File::open(file_path).expect("Failed to open file");
    let mut buffer = Vec::new();
    file.read_to_end(&mut buffer).expect("Failed to read file");
    buffer
}

fn to_json(ipld: Ipld) -> serde_json::Value {
    let json_value = match ipld {
        Ipld::Null => Value::Null,
        Ipld::Bool(boolean) => Value::Bool(boolean),
        Ipld::Integer(integer) => Value::Number({ integer as i64 }.into()),
        Ipld::String(string) => Value::String(string.to_owned()),
        Ipld::Float(float) => {
            Value::Number(serde_json::Number::from_f64(float).expect("64 bit floating point"))
        }
        Ipld::Bytes(byte) => Value::Array(
            byte.into_iter()
                .map(|byte| serde_json::to_value(byte).unwrap())
                .collect(),
        ),
        Ipld::List(list) => {
            let list = list.into_iter().map(to_json).collect::<Value>();
            list
        }
        Ipld::Map(map) => {
            let mut serde_map = serde_json::Map::new();
            for (key, value) in map {
                serde_map.insert(key, to_json(value)); // Recurses into the Map
            }
            Value::Object(serde_map)
        }
        Ipld::Link(cid) => Value::String(cid.to_string()),
    };
    json_value
}

fn gen_random_string() -> String {
    let s: String = rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(7)
        .map(char::from)
        .collect();
    //dbg!(&s);
    format!("{}", s)
}

async fn main() {
    // ########################################
    // Repository Data
    // ########################################

    // loop through every block in the archive

    // Open store
    let store = Store::open("rocks.db").await.unwrap();

    // Create CarReader from repo bytes
    let reader = CarReader::new(&*repo_bytes).await.expect("valid CAR file");
    /*
    while let Ok(Some(block)) = reader.next_block().await {
        let (cid, bytes) = block;

        // TODO: remove this hack
        // car_reader uses cid::Cid::Cid instead of libipld::cid::Cid
        let cid = Cid::from_str(&cid.to_string()).unwrap();

        // Recreate block, probably inefficient
        let block: Block<DefaultParams> = Block::new(cid, bytes.clone())
            .expect("valid block");

        // It's a mixture of jazz and funk, 's called JUNK
        match store.put_block(&block) {
            Ok(_cid) => {
                //println!("Inserted: {}", &cid);
                if let Ok(ipld) = Codec::decode::<Ipld>(&DagCborCodec, &bytes) {
                    if let Ok(commit) = serde_json::from_value::<Commit>(to_json(ipld)) {
                        println!("\n\n\n\nCommit: {:?}", &commit);
                        if let Some(cid_str) = commit.next_node() {
                            let cid = Cid::from_str(&cid_str).unwrap();
                            if let Ok(Some(block)) = store.get_block(cid) {
                                if let Ok(ipld) = Codec::decode::<Ipld>(&DagCborCodec, &block.data()) {
                                    if let Ok(root) = serde_json::from_value::<RootNode>(to_json(ipld)) {
                                        if let Some(cid_str) = root.next_node() {
                                            let cid = Cid::from_str(&cid_str).unwrap();
                                            if let Ok(Some(block)) = store.get_block(cid) {
                                                if let Ok(ipld) = Codec::decode::<Ipld>(&DagCborCodec, &block.data()) {
                                                    if let Ok(mut mst_node) = serde_json::from_value::<MstNode>(to_json(ipld)) {
                                                        // Leaves
                                                        if true {
                                                            while let Some(record) = &mst_node.e.pop() {
                                                                let cid_str = &record.v;
                                                                let cid = Cid::from_str(&cid_str).unwrap();
                                                                if let Ok(Some(block)) = store.get_block(cid) {
                                                                    if let Ok(ipld) = Codec::decode::<Ipld>(&DagCborCodec, &block.data()) {
                                                                        println!("This block is a record:");
                                                                        dbg!(&ipld); // This is where mst recursion meets the lexicon object codegen
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        // Next Branch
                                                        if true {
                                                            if let Some(mst_node) = &mst_node.l {
                                                                dbg!(mst_node);
                                                                // recurse plz
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // store.get(cid).unwrap().unwrap() // Confirm via round trip
            },
            Err(_) => panic!("crashed inserting block to db"),
        };
    }
    */

    // ########################################
    // Polymorphic Lexicon Generation
    // ########################################
    println!("WALKING LEXICON");
    let root = Path::new("lexicons");
    if let Err(e) = walk_lexicon(root) {
        println!("Error: {:?}", e);
    };
}

fn walk_lexicon(dir: &Path) -> Result<(), std::io::Error> {
    if dir.is_dir() {
        for entry in read_dir(dir)? {
            let entry = entry?;
            let path = entry.path();
            if path.is_dir() {
                walk_lexicon(&path);
            } else if path.extension().unwrap() == "json" {
                code_generator::lex2rs(&path);
            }
        }
        return Ok(());
    }
    Ok(()) // TODO: Remove silent failure
}
