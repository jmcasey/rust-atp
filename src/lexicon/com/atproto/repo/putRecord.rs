/// Write a record, creating or updating it as needed.

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct PutRecord {
    collection: String,
    rkey: String,
    validate: Option<bool>,
    swapRecord: Option<String>,
    record: A wild Unknown appeared!,
    repo: String,
    swapCommit: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct PutRecord {
    cid: String,
    uri: String,
}

pub enum Error {
    InvalidSwap,
}