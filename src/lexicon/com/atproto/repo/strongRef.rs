#[derive(Debug, Deserialize, Serialize)]
pub struct StrongRef {
    cid: String,
    uri: String,
}