/// Create a new record.

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct CreateRecord {
    validate: Option<bool>,
    record: A wild Unknown appeared!,
    repo: String,
    rkey: Option<String>,
    collection: String,
    swapCommit: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct CreateRecord {
    uri: String,
    cid: String,
}

pub enum Error {
    InvalidSwap,
}