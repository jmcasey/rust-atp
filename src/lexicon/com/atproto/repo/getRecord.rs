/// Get a record.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    collection: String,
    rkey: String,
    cid: Option<String>,
    repo: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct GetRecord {
    cid: Option<String>,
    value: A wild Unknown appeared!,
    uri: String,
}