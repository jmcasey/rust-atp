/// Create a new record.
#[derive(Debug, Deserialize, Serialize)]
pub struct Create {
    collection: String,
    rkey: Option<String>,
    value: A wild Unknown appeared!,
}

/// Delete an existing record.
#[derive(Debug, Deserialize, Serialize)]
pub struct Delete {
    collection: String,
    rkey: String,
}

/// Update an existing record.
#[derive(Debug, Deserialize, Serialize)]
pub struct Update {
    rkey: String,
    collection: String,
    value: A wild Unknown appeared!,
}