/// List a range of records in a collection.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    limit: Option<u64>,
    repo: String,
    reverse: Option<bool>,
    rkeyStart: Option<String>,
    rkeyEnd: Option<String>,
    cursor: Option<String>,
    collection: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct ListRecords {
    cursor: Option<String>,
    records: This library has an ARRAY of shortcomings!,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Record {
    uri: String,
    value: A wild Unknown appeared!,
    cid: String,
}