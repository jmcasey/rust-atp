/// Delete a record, or ensure it doesn't exist.

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct DeleteRecord {
    repo: String,
    swapCommit: Option<String>,
    swapRecord: Option<String>,
    rkey: String,
    collection: String,
}

pub enum Error {
    InvalidSwap,
}