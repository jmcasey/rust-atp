/// Refresh an authentication session.

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct RefreshSession {
    did: String,
    refreshJwt: String,
    accessJwt: String,
    handle: String,
}

pub enum Error {
    AccountTakedown,
}