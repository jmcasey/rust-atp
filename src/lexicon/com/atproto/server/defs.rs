#[derive(Debug, Deserialize, Serialize)]
pub struct InviteCode {
    code: String,
    forAccount: String,
    uses: This library has an ARRAY of shortcomings!,
    disabled: bool,
    available: u64,
    createdAt: String,
    createdBy: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct InviteCodeUse {
    usedBy: String,
    usedAt: String,
}