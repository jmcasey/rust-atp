/// Reset a user account password using a token.

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct ResetPassword {
    token: String,
    password: String,
}

pub enum Error {
    ExpiredToken,
    InvalidToken,
}