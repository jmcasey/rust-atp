/// Create an authentication session.

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct CreateSession {
    identifier: String,
    password: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct CreateSession {
    did: String,
    accessJwt: String,
    refreshJwt: String,
    handle: String,
    email: Option<String>,
}

pub enum Error {
    AccountTakedown,
}