/// Create an account.

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct CreateAccount {
    inviteCode: Option<String>,
    recoveryKey: Option<String>,
    did: Option<String>,
    handle: String,
    password: String,
    email: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct CreateAccount {
    refreshJwt: String,
    handle: String,
    accessJwt: String,
    did: String,
}

pub enum Error {
    InvalidHandle,
    InvalidPassword,
    InvalidInviteCode,
    HandleNotAvailable,
    UnsupportedDomain,
    UnresolvableDid,
    IncompatibleDidDoc,
}