/// Get information about the current session.

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct GetSession {
    did: String,
    email: Option<String>,
    handle: String,
}