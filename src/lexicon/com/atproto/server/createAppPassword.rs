#[derive(Debug, Deserialize, Serialize)]
pub struct AppPassword {
    createdAt: String,
    name: String,
    password: String,
}

/// Create an app-specific password.

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct AppPassword {
    name: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

pub type AppPassword = #appPassword;
pub enum Error {
    AccountTakedown,
}