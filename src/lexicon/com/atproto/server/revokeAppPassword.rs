/// Revoke an app-specific password by name.

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct RevokeAppPassword {
    name: String,
}