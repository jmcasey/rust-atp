/// Initiate a user account password reset via email.

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct RequestPasswordReset {
    email: String,
}