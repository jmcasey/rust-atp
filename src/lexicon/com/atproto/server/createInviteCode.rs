/// Create an invite code.

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct CreateInviteCode {
    forAccount: Option<String>,
    useCount: u64,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct CreateInviteCode {
    code: String,
}