/// Delete a user account with a token and password.

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct DeleteAccount {
    token: String,
    password: String,
    did: String,
}

pub enum Error {
    ExpiredToken,
    InvalidToken,
}