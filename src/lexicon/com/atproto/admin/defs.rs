/// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
pub struct Acknowledge;

#[derive(Debug, Deserialize, Serialize)]
pub struct ActionReversal {
    createdBy: String,
    reason: String,
    createdAt: String,
}

pub type ActionViewCurrent = #actionType;
#[derive(Debug, Deserialize, Serialize)]
pub struct ActionViewCurrent {
    durationInHours: Option<u64>,
    id: u64,
    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
    pub struct Acknowledge;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionReversal {
        createdBy: String,
        reason: String,
        createdAt: String,
    }

    pub type ActionViewCurrent = #actionType;,
}

/// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
pub struct Escalate;

/// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
pub struct Flag;

#[derive(Debug, Deserialize, Serialize)]
pub struct ImageDetails {
    width: u64,
    height: u64,
}

pub type Moderation = #actionViewCurrent;
#[derive(Debug, Deserialize, Serialize)]
pub struct Moderation {
    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
    pub struct Acknowledge;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionReversal {
        createdBy: String,
        reason: String,
        createdAt: String,
    }

    pub type ActionViewCurrent = #actionType;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionViewCurrent {
        durationInHours: Option<u64>,
        id: u64,
        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;,
    }

    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
    pub struct Escalate;

    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
    pub struct Flag;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ImageDetails {
        width: u64,
        height: u64,
    }

    pub type Moderation = #actionViewCurrent;>,
}

pub type ModerationDetail = #actionViewCurrent;
#[derive(Debug, Deserialize, Serialize)]
pub struct ModerationDetail {
    reports: This library has an ARRAY of shortcomings!,
    actions: This library has an ARRAY of shortcomings!,
    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
    pub struct Acknowledge;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionReversal {
        createdBy: String,
        reason: String,
        createdAt: String,
    }

    pub type ActionViewCurrent = #actionType;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionViewCurrent {
        durationInHours: Option<u64>,
        id: u64,
        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;,
    }

    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
    pub struct Escalate;

    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
    pub struct Flag;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ImageDetails {
        width: u64,
        height: u64,
    }

    pub type Moderation = #actionViewCurrent;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct Moderation {
        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;>,
    }

    pub type ModerationDetail = #actionViewCurrent;>,
}

pub type RecordView = #moderation;
pub type RecordView = #repoView;
#[derive(Debug, Deserialize, Serialize)]
pub struct RecordView {
    indexedAt: String,
    uri: String,
    blobCids: This library has an ARRAY of shortcomings!,
    value: A wild Unknown appeared!,
    moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
    pub struct Acknowledge;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionReversal {
        createdBy: String,
        reason: String,
        createdAt: String,
    }

    pub type ActionViewCurrent = #actionType;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionViewCurrent {
        durationInHours: Option<u64>,
        id: u64,
        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;,
    }

    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
    pub struct Escalate;

    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
    pub struct Flag;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ImageDetails {
        width: u64,
        height: u64,
    }

    pub type Moderation = #actionViewCurrent;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct Moderation {
        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;>,
    }

    pub type ModerationDetail = #actionViewCurrent;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ModerationDetail {
        reports: This library has an ARRAY of shortcomings!,
        actions: This library has an ARRAY of shortcomings!,
        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;>,
    }

    pub type RecordView = #moderation;,
    repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
    pub struct Acknowledge;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionReversal {
        createdBy: String,
        reason: String,
        createdAt: String,
    }

    pub type ActionViewCurrent = #actionType;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionViewCurrent {
        durationInHours: Option<u64>,
        id: u64,
        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;,
    }

    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
    pub struct Escalate;

    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
    pub struct Flag;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ImageDetails {
        width: u64,
        height: u64,
    }

    pub type Moderation = #actionViewCurrent;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct Moderation {
        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;>,
    }

    pub type ModerationDetail = #actionViewCurrent;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ModerationDetail {
        reports: This library has an ARRAY of shortcomings!,
        actions: This library has an ARRAY of shortcomings!,
        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;>,
    }

    pub type RecordView = #moderation;
    pub type RecordView = #repoView;,
    cid: String,
}

pub type RecordViewDetail = #moderationDetail;
pub type RecordViewDetail = #repoView;
#[derive(Debug, Deserialize, Serialize)]
pub struct RecordViewDetail {
    repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
    pub struct Acknowledge;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionReversal {
        createdBy: String,
        reason: String,
        createdAt: String,
    }

    pub type ActionViewCurrent = #actionType;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionViewCurrent {
        durationInHours: Option<u64>,
        id: u64,
        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;,
    }

    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
    pub struct Escalate;

    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
    pub struct Flag;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ImageDetails {
        width: u64,
        height: u64,
    }

    pub type Moderation = #actionViewCurrent;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct Moderation {
        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;>,
    }

    pub type ModerationDetail = #actionViewCurrent;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ModerationDetail {
        reports: This library has an ARRAY of shortcomings!,
        actions: This library has an ARRAY of shortcomings!,
        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;>,
    }

    pub type RecordView = #moderation;
    pub type RecordView = #repoView;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct RecordView {
        indexedAt: String,
        uri: String,
        blobCids: This library has an ARRAY of shortcomings!,
        value: A wild Unknown appeared!,
        moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;,
        repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;
        pub type RecordView = #repoView;,
        cid: String,
    }

    pub type RecordViewDetail = #moderationDetail;
    pub type RecordViewDetail = #repoView;,
    cid: String,
    uri: String,
    moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
    pub struct Acknowledge;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionReversal {
        createdBy: String,
        reason: String,
        createdAt: String,
    }

    pub type ActionViewCurrent = #actionType;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionViewCurrent {
        durationInHours: Option<u64>,
        id: u64,
        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;,
    }

    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
    pub struct Escalate;

    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
    pub struct Flag;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ImageDetails {
        width: u64,
        height: u64,
    }

    pub type Moderation = #actionViewCurrent;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct Moderation {
        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;>,
    }

    pub type ModerationDetail = #actionViewCurrent;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ModerationDetail {
        reports: This library has an ARRAY of shortcomings!,
        actions: This library has an ARRAY of shortcomings!,
        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;>,
    }

    pub type RecordView = #moderation;
    pub type RecordView = #repoView;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct RecordView {
        indexedAt: String,
        uri: String,
        blobCids: This library has an ARRAY of shortcomings!,
        value: A wild Unknown appeared!,
        moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;,
        repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;
        pub type RecordView = #repoView;,
        cid: String,
    }

    pub type RecordViewDetail = #moderationDetail;,
    blobs: This library has an ARRAY of shortcomings!,
    value: A wild Unknown appeared!,
    labels: Option<This library has an ARRAY of shortcomings!>,
    indexedAt: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct RecordViewNotFound {
    uri: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct RepoRef {
    did: String,
}

pub type RepoView = #moderation;
pub type RepoView = com.atproto.server.defs#inviteCode;
#[derive(Debug, Deserialize, Serialize)]
pub struct RepoView {
    inviteNote: Option<String>,
    handle: String,
    did: String,
    invitesDisabled: Option<bool>,
    email: Option<String>,
    relatedRecords: This library has an ARRAY of shortcomings!,
    moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
    pub struct Acknowledge;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionReversal {
        createdBy: String,
        reason: String,
        createdAt: String,
    }

    pub type ActionViewCurrent = #actionType;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionViewCurrent {
        durationInHours: Option<u64>,
        id: u64,
        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;,
    }

    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
    pub struct Escalate;

    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
    pub struct Flag;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ImageDetails {
        width: u64,
        height: u64,
    }

    pub type Moderation = #actionViewCurrent;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct Moderation {
        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;>,
    }

    pub type ModerationDetail = #actionViewCurrent;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ModerationDetail {
        reports: This library has an ARRAY of shortcomings!,
        actions: This library has an ARRAY of shortcomings!,
        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;>,
    }

    pub type RecordView = #moderation;
    pub type RecordView = #repoView;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct RecordView {
        indexedAt: String,
        uri: String,
        blobCids: This library has an ARRAY of shortcomings!,
        value: A wild Unknown appeared!,
        moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;,
        repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;
        pub type RecordView = #repoView;,
        cid: String,
    }

    pub type RecordViewDetail = #moderationDetail;
    pub type RecordViewDetail = #repoView;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct RecordViewDetail {
        repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;
        pub type RecordView = #repoView;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordView {
            indexedAt: String,
            uri: String,
            blobCids: This library has an ARRAY of shortcomings!,
            value: A wild Unknown appeared!,
            moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;,
            repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;,
            cid: String,
        }

        pub type RecordViewDetail = #moderationDetail;
        pub type RecordViewDetail = #repoView;,
        cid: String,
        uri: String,
        moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;
        pub type RecordView = #repoView;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordView {
            indexedAt: String,
            uri: String,
            blobCids: This library has an ARRAY of shortcomings!,
            value: A wild Unknown appeared!,
            moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;,
            repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;,
            cid: String,
        }

        pub type RecordViewDetail = #moderationDetail;,
        blobs: This library has an ARRAY of shortcomings!,
        value: A wild Unknown appeared!,
        labels: Option<This library has an ARRAY of shortcomings!>,
        indexedAt: String,
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct RecordViewNotFound {
        uri: String,
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct RepoRef {
        did: String,
    }

    pub type RepoView = #moderation;,
    indexedAt: String,
    invitedBy: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
    pub struct Acknowledge;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionReversal {
        createdBy: String,
        reason: String,
        createdAt: String,
    }

    pub type ActionViewCurrent = #actionType;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionViewCurrent {
        durationInHours: Option<u64>,
        id: u64,
        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;,
    }

    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
    pub struct Escalate;

    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
    pub struct Flag;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ImageDetails {
        width: u64,
        height: u64,
    }

    pub type Moderation = #actionViewCurrent;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct Moderation {
        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;>,
    }

    pub type ModerationDetail = #actionViewCurrent;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ModerationDetail {
        reports: This library has an ARRAY of shortcomings!,
        actions: This library has an ARRAY of shortcomings!,
        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;>,
    }

    pub type RecordView = #moderation;
    pub type RecordView = #repoView;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct RecordView {
        indexedAt: String,
        uri: String,
        blobCids: This library has an ARRAY of shortcomings!,
        value: A wild Unknown appeared!,
        moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;,
        repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;
        pub type RecordView = #repoView;,
        cid: String,
    }

    pub type RecordViewDetail = #moderationDetail;
    pub type RecordViewDetail = #repoView;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct RecordViewDetail {
        repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;
        pub type RecordView = #repoView;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordView {
            indexedAt: String,
            uri: String,
            blobCids: This library has an ARRAY of shortcomings!,
            value: A wild Unknown appeared!,
            moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;,
            repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;,
            cid: String,
        }

        pub type RecordViewDetail = #moderationDetail;
        pub type RecordViewDetail = #repoView;,
        cid: String,
        uri: String,
        moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;
        pub type RecordView = #repoView;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordView {
            indexedAt: String,
            uri: String,
            blobCids: This library has an ARRAY of shortcomings!,
            value: A wild Unknown appeared!,
            moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;,
            repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;,
            cid: String,
        }

        pub type RecordViewDetail = #moderationDetail;,
        blobs: This library has an ARRAY of shortcomings!,
        value: A wild Unknown appeared!,
        labels: Option<This library has an ARRAY of shortcomings!>,
        indexedAt: String,
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct RecordViewNotFound {
        uri: String,
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct RepoRef {
        did: String,
    }

    pub type RepoView = #moderation;
    pub type RepoView = com.atproto.server.defs#inviteCode;>,
}

pub type RepoViewDetail = com.atproto.server.defs#inviteCode;
pub type RepoViewDetail = #moderationDetail;
#[derive(Debug, Deserialize, Serialize)]
pub struct RepoViewDetail {
    inviteNote: Option<String>,
    did: String,
    labels: Option<This library has an ARRAY of shortcomings!>,
    handle: String,
    email: Option<String>,
    moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
    pub struct Acknowledge;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionReversal {
        createdBy: String,
        reason: String,
        createdAt: String,
    }

    pub type ActionViewCurrent = #actionType;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionViewCurrent {
        durationInHours: Option<u64>,
        id: u64,
        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;,
    }

    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
    pub struct Escalate;

    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
    pub struct Flag;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ImageDetails {
        width: u64,
        height: u64,
    }

    pub type Moderation = #actionViewCurrent;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct Moderation {
        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;>,
    }

    pub type ModerationDetail = #actionViewCurrent;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ModerationDetail {
        reports: This library has an ARRAY of shortcomings!,
        actions: This library has an ARRAY of shortcomings!,
        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;>,
    }

    pub type RecordView = #moderation;
    pub type RecordView = #repoView;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct RecordView {
        indexedAt: String,
        uri: String,
        blobCids: This library has an ARRAY of shortcomings!,
        value: A wild Unknown appeared!,
        moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;,
        repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;
        pub type RecordView = #repoView;,
        cid: String,
    }

    pub type RecordViewDetail = #moderationDetail;
    pub type RecordViewDetail = #repoView;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct RecordViewDetail {
        repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;
        pub type RecordView = #repoView;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordView {
            indexedAt: String,
            uri: String,
            blobCids: This library has an ARRAY of shortcomings!,
            value: A wild Unknown appeared!,
            moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;,
            repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;,
            cid: String,
        }

        pub type RecordViewDetail = #moderationDetail;
        pub type RecordViewDetail = #repoView;,
        cid: String,
        uri: String,
        moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;
        pub type RecordView = #repoView;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordView {
            indexedAt: String,
            uri: String,
            blobCids: This library has an ARRAY of shortcomings!,
            value: A wild Unknown appeared!,
            moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;,
            repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;,
            cid: String,
        }

        pub type RecordViewDetail = #moderationDetail;,
        blobs: This library has an ARRAY of shortcomings!,
        value: A wild Unknown appeared!,
        labels: Option<This library has an ARRAY of shortcomings!>,
        indexedAt: String,
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct RecordViewNotFound {
        uri: String,
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct RepoRef {
        did: String,
    }

    pub type RepoView = #moderation;
    pub type RepoView = com.atproto.server.defs#inviteCode;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct RepoView {
        inviteNote: Option<String>,
        handle: String,
        did: String,
        invitesDisabled: Option<bool>,
        email: Option<String>,
        relatedRecords: This library has an ARRAY of shortcomings!,
        moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;
        pub type RecordView = #repoView;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordView {
            indexedAt: String,
            uri: String,
            blobCids: This library has an ARRAY of shortcomings!,
            value: A wild Unknown appeared!,
            moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;,
            repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;,
            cid: String,
        }

        pub type RecordViewDetail = #moderationDetail;
        pub type RecordViewDetail = #repoView;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordViewDetail {
            repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct RecordView {
                indexedAt: String,
                uri: String,
                blobCids: This library has an ARRAY of shortcomings!,
                value: A wild Unknown appeared!,
                moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ModerationDetail {
                    reports: This library has an ARRAY of shortcomings!,
                    actions: This library has an ARRAY of shortcomings!,
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct Moderation {
                        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;
                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionViewCurrent {
                            durationInHours: Option<u64>,
                            id: u64,
                            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                            pub struct Acknowledge;

                            #[derive(Debug, Deserialize, Serialize)]
                            pub struct ActionReversal {
                                createdBy: String,
                                reason: String,
                                createdAt: String,
                            }

                            pub type ActionViewCurrent = #actionType;,
                        }

                        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                        pub struct Escalate;

                        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                        pub struct Flag;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ImageDetails {
                            width: u64,
                            height: u64,
                        }

                        pub type Moderation = #actionViewCurrent;>,
                    }

                    pub type ModerationDetail = #actionViewCurrent;>,
                }

                pub type RecordView = #moderation;,
                repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ModerationDetail {
                    reports: This library has an ARRAY of shortcomings!,
                    actions: This library has an ARRAY of shortcomings!,
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct Moderation {
                        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;
                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionViewCurrent {
                            durationInHours: Option<u64>,
                            id: u64,
                            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                            pub struct Acknowledge;

                            #[derive(Debug, Deserialize, Serialize)]
                            pub struct ActionReversal {
                                createdBy: String,
                                reason: String,
                                createdAt: String,
                            }

                            pub type ActionViewCurrent = #actionType;,
                        }

                        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                        pub struct Escalate;

                        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                        pub struct Flag;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ImageDetails {
                            width: u64,
                            height: u64,
                        }

                        pub type Moderation = #actionViewCurrent;>,
                    }

                    pub type ModerationDetail = #actionViewCurrent;>,
                }

                pub type RecordView = #moderation;
                pub type RecordView = #repoView;,
                cid: String,
            }

            pub type RecordViewDetail = #moderationDetail;
            pub type RecordViewDetail = #repoView;,
            cid: String,
            uri: String,
            moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct RecordView {
                indexedAt: String,
                uri: String,
                blobCids: This library has an ARRAY of shortcomings!,
                value: A wild Unknown appeared!,
                moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ModerationDetail {
                    reports: This library has an ARRAY of shortcomings!,
                    actions: This library has an ARRAY of shortcomings!,
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct Moderation {
                        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;
                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionViewCurrent {
                            durationInHours: Option<u64>,
                            id: u64,
                            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                            pub struct Acknowledge;

                            #[derive(Debug, Deserialize, Serialize)]
                            pub struct ActionReversal {
                                createdBy: String,
                                reason: String,
                                createdAt: String,
                            }

                            pub type ActionViewCurrent = #actionType;,
                        }

                        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                        pub struct Escalate;

                        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                        pub struct Flag;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ImageDetails {
                            width: u64,
                            height: u64,
                        }

                        pub type Moderation = #actionViewCurrent;>,
                    }

                    pub type ModerationDetail = #actionViewCurrent;>,
                }

                pub type RecordView = #moderation;,
                repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ModerationDetail {
                    reports: This library has an ARRAY of shortcomings!,
                    actions: This library has an ARRAY of shortcomings!,
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct Moderation {
                        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;
                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionViewCurrent {
                            durationInHours: Option<u64>,
                            id: u64,
                            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                            pub struct Acknowledge;

                            #[derive(Debug, Deserialize, Serialize)]
                            pub struct ActionReversal {
                                createdBy: String,
                                reason: String,
                                createdAt: String,
                            }

                            pub type ActionViewCurrent = #actionType;,
                        }

                        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                        pub struct Escalate;

                        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                        pub struct Flag;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ImageDetails {
                            width: u64,
                            height: u64,
                        }

                        pub type Moderation = #actionViewCurrent;>,
                    }

                    pub type ModerationDetail = #actionViewCurrent;>,
                }

                pub type RecordView = #moderation;
                pub type RecordView = #repoView;,
                cid: String,
            }

            pub type RecordViewDetail = #moderationDetail;,
            blobs: This library has an ARRAY of shortcomings!,
            value: A wild Unknown appeared!,
            labels: Option<This library has an ARRAY of shortcomings!>,
            indexedAt: String,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordViewNotFound {
            uri: String,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct RepoRef {
            did: String,
        }

        pub type RepoView = #moderation;,
        indexedAt: String,
        invitedBy: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;
        pub type RecordView = #repoView;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordView {
            indexedAt: String,
            uri: String,
            blobCids: This library has an ARRAY of shortcomings!,
            value: A wild Unknown appeared!,
            moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;,
            repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;,
            cid: String,
        }

        pub type RecordViewDetail = #moderationDetail;
        pub type RecordViewDetail = #repoView;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordViewDetail {
            repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct RecordView {
                indexedAt: String,
                uri: String,
                blobCids: This library has an ARRAY of shortcomings!,
                value: A wild Unknown appeared!,
                moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ModerationDetail {
                    reports: This library has an ARRAY of shortcomings!,
                    actions: This library has an ARRAY of shortcomings!,
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct Moderation {
                        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;
                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionViewCurrent {
                            durationInHours: Option<u64>,
                            id: u64,
                            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                            pub struct Acknowledge;

                            #[derive(Debug, Deserialize, Serialize)]
                            pub struct ActionReversal {
                                createdBy: String,
                                reason: String,
                                createdAt: String,
                            }

                            pub type ActionViewCurrent = #actionType;,
                        }

                        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                        pub struct Escalate;

                        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                        pub struct Flag;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ImageDetails {
                            width: u64,
                            height: u64,
                        }

                        pub type Moderation = #actionViewCurrent;>,
                    }

                    pub type ModerationDetail = #actionViewCurrent;>,
                }

                pub type RecordView = #moderation;,
                repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ModerationDetail {
                    reports: This library has an ARRAY of shortcomings!,
                    actions: This library has an ARRAY of shortcomings!,
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct Moderation {
                        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;
                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionViewCurrent {
                            durationInHours: Option<u64>,
                            id: u64,
                            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                            pub struct Acknowledge;

                            #[derive(Debug, Deserialize, Serialize)]
                            pub struct ActionReversal {
                                createdBy: String,
                                reason: String,
                                createdAt: String,
                            }

                            pub type ActionViewCurrent = #actionType;,
                        }

                        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                        pub struct Escalate;

                        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                        pub struct Flag;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ImageDetails {
                            width: u64,
                            height: u64,
                        }

                        pub type Moderation = #actionViewCurrent;>,
                    }

                    pub type ModerationDetail = #actionViewCurrent;>,
                }

                pub type RecordView = #moderation;
                pub type RecordView = #repoView;,
                cid: String,
            }

            pub type RecordViewDetail = #moderationDetail;
            pub type RecordViewDetail = #repoView;,
            cid: String,
            uri: String,
            moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct RecordView {
                indexedAt: String,
                uri: String,
                blobCids: This library has an ARRAY of shortcomings!,
                value: A wild Unknown appeared!,
                moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ModerationDetail {
                    reports: This library has an ARRAY of shortcomings!,
                    actions: This library has an ARRAY of shortcomings!,
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct Moderation {
                        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;
                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionViewCurrent {
                            durationInHours: Option<u64>,
                            id: u64,
                            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                            pub struct Acknowledge;

                            #[derive(Debug, Deserialize, Serialize)]
                            pub struct ActionReversal {
                                createdBy: String,
                                reason: String,
                                createdAt: String,
                            }

                            pub type ActionViewCurrent = #actionType;,
                        }

                        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                        pub struct Escalate;

                        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                        pub struct Flag;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ImageDetails {
                            width: u64,
                            height: u64,
                        }

                        pub type Moderation = #actionViewCurrent;>,
                    }

                    pub type ModerationDetail = #actionViewCurrent;>,
                }

                pub type RecordView = #moderation;,
                repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ModerationDetail {
                    reports: This library has an ARRAY of shortcomings!,
                    actions: This library has an ARRAY of shortcomings!,
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct Moderation {
                        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;
                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionViewCurrent {
                            durationInHours: Option<u64>,
                            id: u64,
                            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                            pub struct Acknowledge;

                            #[derive(Debug, Deserialize, Serialize)]
                            pub struct ActionReversal {
                                createdBy: String,
                                reason: String,
                                createdAt: String,
                            }

                            pub type ActionViewCurrent = #actionType;,
                        }

                        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                        pub struct Escalate;

                        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                        pub struct Flag;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ImageDetails {
                            width: u64,
                            height: u64,
                        }

                        pub type Moderation = #actionViewCurrent;>,
                    }

                    pub type ModerationDetail = #actionViewCurrent;>,
                }

                pub type RecordView = #moderation;
                pub type RecordView = #repoView;,
                cid: String,
            }

            pub type RecordViewDetail = #moderationDetail;,
            blobs: This library has an ARRAY of shortcomings!,
            value: A wild Unknown appeared!,
            labels: Option<This library has an ARRAY of shortcomings!>,
            indexedAt: String,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordViewNotFound {
            uri: String,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct RepoRef {
            did: String,
        }

        pub type RepoView = #moderation;
        pub type RepoView = com.atproto.server.defs#inviteCode;>,
    }

    pub type RepoViewDetail = com.atproto.server.defs#inviteCode;
    pub type RepoViewDetail = #moderationDetail;,
    invites: Option<This library has an ARRAY of shortcomings!>,
    invitedBy: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
    pub struct Acknowledge;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionReversal {
        createdBy: String,
        reason: String,
        createdAt: String,
    }

    pub type ActionViewCurrent = #actionType;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ActionViewCurrent {
        durationInHours: Option<u64>,
        id: u64,
        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;,
    }

    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
    pub struct Escalate;

    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
    pub struct Flag;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ImageDetails {
        width: u64,
        height: u64,
    }

    pub type Moderation = #actionViewCurrent;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct Moderation {
        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;>,
    }

    pub type ModerationDetail = #actionViewCurrent;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ModerationDetail {
        reports: This library has an ARRAY of shortcomings!,
        actions: This library has an ARRAY of shortcomings!,
        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;>,
    }

    pub type RecordView = #moderation;
    pub type RecordView = #repoView;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct RecordView {
        indexedAt: String,
        uri: String,
        blobCids: This library has an ARRAY of shortcomings!,
        value: A wild Unknown appeared!,
        moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;,
        repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;
        pub type RecordView = #repoView;,
        cid: String,
    }

    pub type RecordViewDetail = #moderationDetail;
    pub type RecordViewDetail = #repoView;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct RecordViewDetail {
        repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;
        pub type RecordView = #repoView;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordView {
            indexedAt: String,
            uri: String,
            blobCids: This library has an ARRAY of shortcomings!,
            value: A wild Unknown appeared!,
            moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;,
            repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;,
            cid: String,
        }

        pub type RecordViewDetail = #moderationDetail;
        pub type RecordViewDetail = #repoView;,
        cid: String,
        uri: String,
        moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;
        pub type RecordView = #repoView;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordView {
            indexedAt: String,
            uri: String,
            blobCids: This library has an ARRAY of shortcomings!,
            value: A wild Unknown appeared!,
            moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;,
            repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;,
            cid: String,
        }

        pub type RecordViewDetail = #moderationDetail;,
        blobs: This library has an ARRAY of shortcomings!,
        value: A wild Unknown appeared!,
        labels: Option<This library has an ARRAY of shortcomings!>,
        indexedAt: String,
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct RecordViewNotFound {
        uri: String,
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct RepoRef {
        did: String,
    }

    pub type RepoView = #moderation;
    pub type RepoView = com.atproto.server.defs#inviteCode;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct RepoView {
        inviteNote: Option<String>,
        handle: String,
        did: String,
        invitesDisabled: Option<bool>,
        email: Option<String>,
        relatedRecords: This library has an ARRAY of shortcomings!,
        moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;
        pub type RecordView = #repoView;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordView {
            indexedAt: String,
            uri: String,
            blobCids: This library has an ARRAY of shortcomings!,
            value: A wild Unknown appeared!,
            moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;,
            repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;,
            cid: String,
        }

        pub type RecordViewDetail = #moderationDetail;
        pub type RecordViewDetail = #repoView;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordViewDetail {
            repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct RecordView {
                indexedAt: String,
                uri: String,
                blobCids: This library has an ARRAY of shortcomings!,
                value: A wild Unknown appeared!,
                moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ModerationDetail {
                    reports: This library has an ARRAY of shortcomings!,
                    actions: This library has an ARRAY of shortcomings!,
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct Moderation {
                        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;
                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionViewCurrent {
                            durationInHours: Option<u64>,
                            id: u64,
                            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                            pub struct Acknowledge;

                            #[derive(Debug, Deserialize, Serialize)]
                            pub struct ActionReversal {
                                createdBy: String,
                                reason: String,
                                createdAt: String,
                            }

                            pub type ActionViewCurrent = #actionType;,
                        }

                        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                        pub struct Escalate;

                        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                        pub struct Flag;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ImageDetails {
                            width: u64,
                            height: u64,
                        }

                        pub type Moderation = #actionViewCurrent;>,
                    }

                    pub type ModerationDetail = #actionViewCurrent;>,
                }

                pub type RecordView = #moderation;,
                repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ModerationDetail {
                    reports: This library has an ARRAY of shortcomings!,
                    actions: This library has an ARRAY of shortcomings!,
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct Moderation {
                        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;
                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionViewCurrent {
                            durationInHours: Option<u64>,
                            id: u64,
                            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                            pub struct Acknowledge;

                            #[derive(Debug, Deserialize, Serialize)]
                            pub struct ActionReversal {
                                createdBy: String,
                                reason: String,
                                createdAt: String,
                            }

                            pub type ActionViewCurrent = #actionType;,
                        }

                        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                        pub struct Escalate;

                        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                        pub struct Flag;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ImageDetails {
                            width: u64,
                            height: u64,
                        }

                        pub type Moderation = #actionViewCurrent;>,
                    }

                    pub type ModerationDetail = #actionViewCurrent;>,
                }

                pub type RecordView = #moderation;
                pub type RecordView = #repoView;,
                cid: String,
            }

            pub type RecordViewDetail = #moderationDetail;
            pub type RecordViewDetail = #repoView;,
            cid: String,
            uri: String,
            moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct RecordView {
                indexedAt: String,
                uri: String,
                blobCids: This library has an ARRAY of shortcomings!,
                value: A wild Unknown appeared!,
                moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ModerationDetail {
                    reports: This library has an ARRAY of shortcomings!,
                    actions: This library has an ARRAY of shortcomings!,
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct Moderation {
                        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;
                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionViewCurrent {
                            durationInHours: Option<u64>,
                            id: u64,
                            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                            pub struct Acknowledge;

                            #[derive(Debug, Deserialize, Serialize)]
                            pub struct ActionReversal {
                                createdBy: String,
                                reason: String,
                                createdAt: String,
                            }

                            pub type ActionViewCurrent = #actionType;,
                        }

                        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                        pub struct Escalate;

                        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                        pub struct Flag;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ImageDetails {
                            width: u64,
                            height: u64,
                        }

                        pub type Moderation = #actionViewCurrent;>,
                    }

                    pub type ModerationDetail = #actionViewCurrent;>,
                }

                pub type RecordView = #moderation;,
                repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ModerationDetail {
                    reports: This library has an ARRAY of shortcomings!,
                    actions: This library has an ARRAY of shortcomings!,
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct Moderation {
                        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;
                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionViewCurrent {
                            durationInHours: Option<u64>,
                            id: u64,
                            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                            pub struct Acknowledge;

                            #[derive(Debug, Deserialize, Serialize)]
                            pub struct ActionReversal {
                                createdBy: String,
                                reason: String,
                                createdAt: String,
                            }

                            pub type ActionViewCurrent = #actionType;,
                        }

                        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                        pub struct Escalate;

                        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                        pub struct Flag;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ImageDetails {
                            width: u64,
                            height: u64,
                        }

                        pub type Moderation = #actionViewCurrent;>,
                    }

                    pub type ModerationDetail = #actionViewCurrent;>,
                }

                pub type RecordView = #moderation;
                pub type RecordView = #repoView;,
                cid: String,
            }

            pub type RecordViewDetail = #moderationDetail;,
            blobs: This library has an ARRAY of shortcomings!,
            value: A wild Unknown appeared!,
            labels: Option<This library has an ARRAY of shortcomings!>,
            indexedAt: String,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordViewNotFound {
            uri: String,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct RepoRef {
            did: String,
        }

        pub type RepoView = #moderation;,
        indexedAt: String,
        invitedBy: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
        pub struct Acknowledge;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionReversal {
            createdBy: String,
            reason: String,
            createdAt: String,
        }

        pub type ActionViewCurrent = #actionType;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ActionViewCurrent {
            durationInHours: Option<u64>,
            id: u64,
            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;,
        }

        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
        pub struct Escalate;

        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
        pub struct Flag;

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ImageDetails {
            width: u64,
            height: u64,
        }

        pub type Moderation = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct Moderation {
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;>,
        }

        pub type ModerationDetail = #actionViewCurrent;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ModerationDetail {
            reports: This library has an ARRAY of shortcomings!,
            actions: This library has an ARRAY of shortcomings!,
            currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;>,
        }

        pub type RecordView = #moderation;
        pub type RecordView = #repoView;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordView {
            indexedAt: String,
            uri: String,
            blobCids: This library has an ARRAY of shortcomings!,
            value: A wild Unknown appeared!,
            moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;,
            repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;,
            cid: String,
        }

        pub type RecordViewDetail = #moderationDetail;
        pub type RecordViewDetail = #repoView;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordViewDetail {
            repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct RecordView {
                indexedAt: String,
                uri: String,
                blobCids: This library has an ARRAY of shortcomings!,
                value: A wild Unknown appeared!,
                moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ModerationDetail {
                    reports: This library has an ARRAY of shortcomings!,
                    actions: This library has an ARRAY of shortcomings!,
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct Moderation {
                        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;
                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionViewCurrent {
                            durationInHours: Option<u64>,
                            id: u64,
                            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                            pub struct Acknowledge;

                            #[derive(Debug, Deserialize, Serialize)]
                            pub struct ActionReversal {
                                createdBy: String,
                                reason: String,
                                createdAt: String,
                            }

                            pub type ActionViewCurrent = #actionType;,
                        }

                        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                        pub struct Escalate;

                        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                        pub struct Flag;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ImageDetails {
                            width: u64,
                            height: u64,
                        }

                        pub type Moderation = #actionViewCurrent;>,
                    }

                    pub type ModerationDetail = #actionViewCurrent;>,
                }

                pub type RecordView = #moderation;,
                repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ModerationDetail {
                    reports: This library has an ARRAY of shortcomings!,
                    actions: This library has an ARRAY of shortcomings!,
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct Moderation {
                        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;
                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionViewCurrent {
                            durationInHours: Option<u64>,
                            id: u64,
                            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                            pub struct Acknowledge;

                            #[derive(Debug, Deserialize, Serialize)]
                            pub struct ActionReversal {
                                createdBy: String,
                                reason: String,
                                createdAt: String,
                            }

                            pub type ActionViewCurrent = #actionType;,
                        }

                        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                        pub struct Escalate;

                        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                        pub struct Flag;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ImageDetails {
                            width: u64,
                            height: u64,
                        }

                        pub type Moderation = #actionViewCurrent;>,
                    }

                    pub type ModerationDetail = #actionViewCurrent;>,
                }

                pub type RecordView = #moderation;
                pub type RecordView = #repoView;,
                cid: String,
            }

            pub type RecordViewDetail = #moderationDetail;
            pub type RecordViewDetail = #repoView;,
            cid: String,
            uri: String,
            moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
            pub struct Acknowledge;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionReversal {
                createdBy: String,
                reason: String,
                createdAt: String,
            }

            pub type ActionViewCurrent = #actionType;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ActionViewCurrent {
                durationInHours: Option<u64>,
                id: u64,
                action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;,
            }

            /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
            pub struct Escalate;

            /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
            pub struct Flag;

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ImageDetails {
                width: u64,
                height: u64,
            }

            pub type Moderation = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct Moderation {
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;>,
            }

            pub type ModerationDetail = #actionViewCurrent;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ModerationDetail {
                reports: This library has an ARRAY of shortcomings!,
                actions: This library has an ARRAY of shortcomings!,
                currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;>,
            }

            pub type RecordView = #moderation;
            pub type RecordView = #repoView;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct RecordView {
                indexedAt: String,
                uri: String,
                blobCids: This library has an ARRAY of shortcomings!,
                value: A wild Unknown appeared!,
                moderation: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ModerationDetail {
                    reports: This library has an ARRAY of shortcomings!,
                    actions: This library has an ARRAY of shortcomings!,
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct Moderation {
                        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;
                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionViewCurrent {
                            durationInHours: Option<u64>,
                            id: u64,
                            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                            pub struct Acknowledge;

                            #[derive(Debug, Deserialize, Serialize)]
                            pub struct ActionReversal {
                                createdBy: String,
                                reason: String,
                                createdAt: String,
                            }

                            pub type ActionViewCurrent = #actionType;,
                        }

                        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                        pub struct Escalate;

                        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                        pub struct Flag;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ImageDetails {
                            width: u64,
                            height: u64,
                        }

                        pub type Moderation = #actionViewCurrent;>,
                    }

                    pub type ModerationDetail = #actionViewCurrent;>,
                }

                pub type RecordView = #moderation;,
                repo: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                pub struct Acknowledge;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionReversal {
                    createdBy: String,
                    reason: String,
                    createdAt: String,
                }

                pub type ActionViewCurrent = #actionType;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ActionViewCurrent {
                    durationInHours: Option<u64>,
                    id: u64,
                    action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;,
                }

                /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                pub struct Escalate;

                /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                pub struct Flag;

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ImageDetails {
                    width: u64,
                    height: u64,
                }

                pub type Moderation = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct Moderation {
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;>,
                }

                pub type ModerationDetail = #actionViewCurrent;
                #[derive(Debug, Deserialize, Serialize)]
                pub struct ModerationDetail {
                    reports: This library has an ARRAY of shortcomings!,
                    actions: This library has an ARRAY of shortcomings!,
                    currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                    pub struct Acknowledge;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionReversal {
                        createdBy: String,
                        reason: String,
                        createdAt: String,
                    }

                    pub type ActionViewCurrent = #actionType;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ActionViewCurrent {
                        durationInHours: Option<u64>,
                        id: u64,
                        action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;,
                    }

                    /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                    pub struct Escalate;

                    /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                    pub struct Flag;

                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct ImageDetails {
                        width: u64,
                        height: u64,
                    }

                    pub type Moderation = #actionViewCurrent;
                    #[derive(Debug, Deserialize, Serialize)]
                    pub struct Moderation {
                        currentAction: Option</// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                        pub struct Acknowledge;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionReversal {
                            createdBy: String,
                            reason: String,
                            createdAt: String,
                        }

                        pub type ActionViewCurrent = #actionType;
                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ActionViewCurrent {
                            durationInHours: Option<u64>,
                            id: u64,
                            action: /// Moderation action type: Acknowledge. Indicates that the content was reviewed and not considered to violate PDS rules.
                            pub struct Acknowledge;

                            #[derive(Debug, Deserialize, Serialize)]
                            pub struct ActionReversal {
                                createdBy: String,
                                reason: String,
                                createdAt: String,
                            }

                            pub type ActionViewCurrent = #actionType;,
                        }

                        /// Moderation action type: Escalate. Indicates that the content has been flagged for additional review.
                        pub struct Escalate;

                        /// Moderation action type: Flag. Indicates that the content was reviewed and considered to violate PDS rules, but may still be served.
                        pub struct Flag;

                        #[derive(Debug, Deserialize, Serialize)]
                        pub struct ImageDetails {
                            width: u64,
                            height: u64,
                        }

                        pub type Moderation = #actionViewCurrent;>,
                    }

                    pub type ModerationDetail = #actionViewCurrent;>,
                }

                pub type RecordView = #moderation;
                pub type RecordView = #repoView;,
                cid: String,
            }

            pub type RecordViewDetail = #moderationDetail;,
            blobs: This library has an ARRAY of shortcomings!,
            value: A wild Unknown appeared!,
            labels: Option<This library has an ARRAY of shortcomings!>,
            indexedAt: String,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct RecordViewNotFound {
            uri: String,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct RepoRef {
            did: String,
        }

        pub type RepoView = #moderation;
        pub type RepoView = com.atproto.server.defs#inviteCode;>,
    }

    pub type RepoViewDetail = com.atproto.server.defs#inviteCode;>,
    relatedRecords: This library has an ARRAY of shortcomings!,
    invitesDisabled: Option<bool>,
    indexedAt: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct RepoViewNotFound {
    did: String,
}

/// Moderation action type: Takedown. Indicates that content should not be served by the PDS.
pub struct Takedown;

#[derive(Debug, Deserialize, Serialize)]
pub struct VideoDetails {
    length: u64,
    width: u64,
    height: u64,
}