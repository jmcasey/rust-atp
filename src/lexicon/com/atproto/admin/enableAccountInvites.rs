/// Re-enable an accounts ability to receive invite codes

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct EnableAccountInvites {
    note: Option<String>,
    account: String,
}