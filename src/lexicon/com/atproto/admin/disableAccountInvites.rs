/// Disable an account from receiving new invite codes, but does not invalidate existing codes

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct DisableAccountInvites {
    account: String,
    note: Option<String>,
}