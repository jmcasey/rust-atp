/// Send email to a user's primary email address

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct SendEmail {
    recipientDid: String,
    subject: Option<String>,
    content: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct SendEmail {
    sent: bool,
}