/// Administrative action to update an account's handle

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct UpdateAccountHandle {
    handle: String,
    did: String,
}