/// Reverse a moderation action.

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct ReverseModerationAction {
    reason: String,
    id: u64,
    createdBy: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

pub type ReverseModerationAction = com.atproto.admin.defs#actionView;