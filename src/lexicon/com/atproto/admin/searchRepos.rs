/// Find repositories based on a search term.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    term: Option<String>,
    cursor: Option<String>,
    limit: Option<u64>,
    invitedBy: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct SearchRepos {
    repos: This library has an ARRAY of shortcomings!,
    cursor: Option<String>,
}