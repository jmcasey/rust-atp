/// List moderation actions related to a subject.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    limit: Option<u64>,
    subject: Option<String>,
    cursor: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct GetModerationActions {
    cursor: Option<String>,
    actions: This library has an ARRAY of shortcomings!,
}