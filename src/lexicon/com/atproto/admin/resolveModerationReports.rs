/// Resolve moderation reports by an action.

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct ResolveModerationReports {
    reportIds: This library has an ARRAY of shortcomings!,
    actionId: u64,
    createdBy: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

pub type ResolveModerationReports = com.atproto.admin.defs#actionView;