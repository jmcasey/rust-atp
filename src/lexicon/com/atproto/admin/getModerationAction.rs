/// View details about a moderation action.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    id: u64,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

pub type GetModerationAction = com.atproto.admin.defs#actionViewDetail;