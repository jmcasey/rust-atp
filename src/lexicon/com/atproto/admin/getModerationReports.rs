/// List moderation reports related to a subject.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    reverse: Option<bool>,
    reporters: Option<This library has an ARRAY of shortcomings!>,
    limit: Option<u64>,
    actionType: Option<String>,
    cursor: Option<String>,
    actionedBy: Option<String>,
    resolved: Option<bool>,
    subject: Option<String>,
    ignoreSubjects: Option<This library has an ARRAY of shortcomings!>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct GetModerationReports {
    reports: This library has an ARRAY of shortcomings!,
    cursor: Option<String>,
}