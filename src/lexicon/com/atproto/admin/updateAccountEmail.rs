/// Administrative action to update an account's email

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct UpdateAccountEmail {
    email: String,
    account: String,
}