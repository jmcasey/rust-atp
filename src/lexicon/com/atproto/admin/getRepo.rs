/// View details about a repository.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    did: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

pub type GetRepo = com.atproto.admin.defs#repoViewDetail;
pub enum Error {
    RepoNotFound,
}