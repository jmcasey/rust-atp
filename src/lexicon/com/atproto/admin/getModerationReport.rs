/// View details about a moderation report.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    id: u64,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

pub type GetModerationReport = com.atproto.admin.defs#reportViewDetail;