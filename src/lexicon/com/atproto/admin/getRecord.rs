/// View details about a record.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    cid: Option<String>,
    uri: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

pub type GetRecord = com.atproto.admin.defs#recordViewDetail;
pub enum Error {
    RecordNotFound,
}