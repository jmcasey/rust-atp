/// Metadata tag on an atproto resource (eg, repo or record)
#[derive(Debug, Deserialize, Serialize)]
pub struct Label {
    cts: String,
    uri: String,
    neg: Option<bool>,
    src: String,
    cid: Option<String>,
    val: String,
}

/// Metadata tag on an atproto record, published by the author within the record. Note -- schemas should use #selfLabels, not #selfLabel.
#[derive(Debug, Deserialize, Serialize)]
pub struct SelfLabel {
    val: String,
}

/// Metadata tags on an atproto record, published by the author within the record.
#[derive(Debug, Deserialize, Serialize)]
pub struct SelfLabels {
    values: This library has an ARRAY of shortcomings!,
}