/// Misleading identity, affiliation, or content
pub struct ReasonMisleading;

/// Other: reports not falling under another report category
pub struct ReasonOther;

/// Rude, harassing, explicit, or otherwise unwelcoming behavior
pub struct ReasonRude;

/// Unwanted or mislabeled sexual content
pub struct ReasonSexual;

/// Spam: frequent unwanted promotion, replies, mentions
pub struct ReasonSpam;

/// Direct violation of server rules, laws, terms of service
pub struct ReasonViolation;