/// Notify a crawling service of a recent update. Often when a long break between updates causes the connection with the crawling service to break.

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct NotifyOfUpdate {
    hostname: String,
}