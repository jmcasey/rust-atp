/// Request a service to persistently crawl hosted repos.

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct RequestCrawl {
    hostname: String,
}