/// Get a blob associated with a given repo.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    did: String,
    cid: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;