/// DEPRECATED - please use com.atproto.sync.getRepo instead

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    did: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;