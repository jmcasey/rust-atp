/// DEPRECATED - please use com.atproto.sync.getLatestCommit instead

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    did: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct GetHead {
    root: String,
}

pub enum Error {
    HeadNotFound,
}