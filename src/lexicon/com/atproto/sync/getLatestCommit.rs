/// Gets the current commit CID & revision of the repo.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    did: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct GetLatestCommit {
    cid: String,
    rev: String,
}

pub enum Error {
    RepoNotFound,
}