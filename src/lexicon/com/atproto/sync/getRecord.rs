/// Gets blocks needed for existence or non-existence of record.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    did: String,
    commit: Option<String>,
    rkey: String,
    collection: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;