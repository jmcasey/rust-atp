/// List blob cids since some revision

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    limit: Option<u64>,
    cursor: Option<String>,
    did: String,
    since: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct ListBlobs {
    cursor: Option<String>,
    cids: This library has an ARRAY of shortcomings!,
}