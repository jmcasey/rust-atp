/// Gets the did's repo, optionally catching up from a specific revision.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    did: String,
    since: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;