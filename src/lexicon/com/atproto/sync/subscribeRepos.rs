#[derive(Debug, Deserialize, Serialize)]
pub struct Commit {
    blocks: [u8],
    prev: Option<String>,
    repo: String,
    blobs: This library has an ARRAY of shortcomings!,
    commit: String,
    rebase: bool,
    seq: u64,
    since: String,
    time: String,
    ops: This library has an ARRAY of shortcomings!,
    rev: String,
    tooBig: bool,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Handle {
    did: String,
    handle: String,
    time: String,
    seq: u64,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Info {
    name: String,
    message: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Migrate {
    did: String,
    seq: u64,
    time: String,
    migrateTo: String,
}

/// A repo operation, ie a write of a single record. For creates and updates, cid is the record's CID as of this operation. For deletes, it's null.
#[derive(Debug, Deserialize, Serialize)]
pub struct RepoOp {
    action: String,
    path: String,
    cid: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Tombstone {
    did: String,
    time: String,
    seq: u64,
}