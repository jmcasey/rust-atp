/// List dids and root cids of hosted repos

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    cursor: Option<String>,
    limit: Option<u64>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct ListRepos {
    cursor: Option<String>,
    repos: This library has an ARRAY of shortcomings!,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Repo {
    head: String,
    did: String,
}