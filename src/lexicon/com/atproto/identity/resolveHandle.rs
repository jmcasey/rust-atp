/// Provides the DID of a repo.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    handle: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct ResolveHandle {
    did: String,
}