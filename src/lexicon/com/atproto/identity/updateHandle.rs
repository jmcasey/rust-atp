/// Updates the handle of the account

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct UpdateHandle {
    handle: String,
}