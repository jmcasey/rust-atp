use crate::lexicon::types::meta::r#ref::LexRef;
use crate::lexicon::types::containers::*;
use crate::lexicon::types::meta::*;
use crate::lexicon::types::primary::*;
use crate::lexicon::types::primatives::*;
use codegen::Scope;
use serde::{Deserialize, Serialize};
use serde_json::Map;
use serde_json::Value;

#[derive(Deserialize, Serialize)]
pub struct Lexicon {
    pub lexicon: i64,
    pub id: String, // NSID
    pub revision: Option<i64>,
    pub description: Option<String>,
    #[serde(flatten)]
    pub defs: Map<String, Value>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(tag = "type")]
pub enum LexiconType {
    #[serde(rename = "query")]
    Query(LexXrpcQuery),
    #[serde(rename = "procedure")]
    Procedure(LexXrpcProcedure),
    #[serde(rename = "subscription")]
    Subscription(LexXrpcSubscription),
    #[serde(rename = "array")]
    Array(LexArray),
    #[serde(rename = "object")]
    Object(LexObject),
    #[serde(rename = "params")]
    Params(LexParams),
    #[serde(rename = "token")]
    Token(LexToken),
    #[serde(rename = "record")]
    Record(LexRecord),
    #[serde(rename = "union")]
    Union(LexUnion),
    #[serde(rename = "ref")]
    Ref(LexRef),
    #[serde(rename = "null")]
    Null,
    #[serde(rename = "boolean")]
    Bool(LexBoolean),
    #[serde(rename = "integer")]
    Integer(LexInteger),
    #[serde(rename = "string")]
    String(LexString),
    #[serde(rename = "bytes")]
    Bytes(LexBytes),
    #[serde(rename = "cid-link")]
    Link(LexCidLink),
    #[serde(rename = "blob")]
    Blob(LexBlob),
    #[serde(rename = "unknown")]
    Unknown(LexUnknown),
}

pub trait ToRust {
    fn to_rust(&self, scope: &mut Scope, name: &str) -> String;
}

impl LexiconType {
    pub fn to_rust(&self, scope: &mut Scope, name: &str) -> String {
        match self {
            LexiconType::Query(q) => q.to_rust(scope, name),
            LexiconType::Procedure(p) => p.to_rust(scope, name),
            LexiconType::Subscription(s) => s.to_rust(scope, name),
            LexiconType::Array(_) => String::from("This library has an ARRAY of shortcomings!"),
            LexiconType::Object(o) => o.to_rust(scope, name),
            LexiconType::Params(p) => p.to_rust(scope, name),
            LexiconType::Token(t) => t.to_rust(scope, name),
            LexiconType::Record(r) => r.to_rust(scope, name),
            LexiconType::Union(u) => u.to_rust(scope, name),
            LexiconType::Ref(r) => r.to_rust(scope, name),
            LexiconType::Null => String::from("Null encountered!"),
            LexiconType::Bool(b) => b.to_rust(scope, name),
            LexiconType::Integer(i) => i.to_rust(scope, name),
            LexiconType::String(s) => s.to_rust(scope, name),
            LexiconType::Bytes(b) => b.to_rust(scope, name),
            LexiconType::Link(l) => l.to_rust(scope, name),
            LexiconType::Blob(b) => b.to_rust(scope, name),
            LexiconType::Unknown(_) => String::from("A wild Unknown appeared!"),
        }
    }
}

pub trait UserType {
    fn r#type(&self) -> &'static str;
}

impl UserType for LexiconType {
    fn r#type(&self) -> &'static str {
        match self {
            LexiconType::Bool(_) => "boolean",
            LexiconType::Integer(_) => "integer",
            LexiconType::String(_) => "string",
            LexiconType::Link(_) => "cid-link",
            LexiconType::Blob(_) => "blob",
            LexiconType::Bytes(_) => "bytes",
            LexiconType::Null => "null",
            LexiconType::Query(_) => "query",
            LexiconType::Procedure(_) => "procedure",
            LexiconType::Subscription(_) => "subscription",
            LexiconType::Array(_) => "array",
            LexiconType::Union(_) => "union",
            LexiconType::Ref(_) => "ref",
            LexiconType::Object(_) => "object",
            LexiconType::Params(_) => "params",
            LexiconType::Record(_) => "record",
            LexiconType::Token(_) => "token",
            LexiconType::Unknown(_) => "unknown",
        }
    }
}

pub trait RustType {
    fn rust_type(&self) -> &'static str;
}

impl RustType for LexiconType {
    fn rust_type(&self) -> &'static str {
        match self {
            LexiconType::Bool(_) => "LexBoolean",
            LexiconType::Integer(_) => "LexInteger",
            LexiconType::String(_) => "LexString",
            LexiconType::Link(_) => "LexRef",
            LexiconType::Blob(_) => "LexBlob",
            LexiconType::Bytes(_) => "LexB",
            LexiconType::Null => "LexNull",
            LexiconType::Query(_) => "LexXrpcQuery",
            LexiconType::Procedure(_) => "LexXrpcQrocedure",
            LexiconType::Subscription(_) => "LexSubscription",
            LexiconType::Array(_) => "LexArray",
            LexiconType::Union(_) => "LexUnion",
            LexiconType::Ref(_) => "LexRef",
            LexiconType::Object(_) => "LexObject",
            LexiconType::Params(_) => "LexParams",
            LexiconType::Record(_) => "LexRecord",
            LexiconType::Unknown(_) => "LexUnknown",
            LexiconType::Token(_) => "LexToken",
        }
    }
}
