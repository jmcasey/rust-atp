use crate::lexicon::core::*;

use codegen::Scope;
use convert_case::{Case, Casing};
use log::debug;
use log::info;
use nsid::Nsid;
use std::error::Error;
use std::fs::create_dir_all;
use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::path::Path;
use std::path::PathBuf;
use std::result::Result;
use std::str::FromStr;

// TODO: this should return a string or a scope and allow the calling context to do file stuff
pub fn lex2rs(root: &Path) -> Result<(), Box<dyn Error>> {
    // read file from os
    let mut file = File::open(root).expect("failed to open lexicon definition file");
    let mut buffer = Vec::new();
    let _ = file
        .read_to_end(&mut buffer)
        .expect("failed to read lexicon definition file");
    let doc: Lexicon = serde_json::from_slice(&buffer).expect("valid json");

    // construct nsid
    let mut scope = Scope::new();
    let nsid = Nsid::from_str(&doc.id).expect("method should be a valid nsid");
    let mut name = nsid.name().to_case(Case::Pascal);

    for (_, definition) in doc.defs {
        if let Some(definition) = definition.as_object() {
            for (frag, def_obj) in definition.iter() {
                println!("Processing {}#{}...", &doc.id, frag);
                match serde_json::from_value::<LexiconType>(def_obj.clone()) {
                    Ok(definition) => {
                        if frag != "main" {
                            name = frag.to_case(Case::Pascal).to_string();
                        }
                        let _ = generate(&mut scope, &name, definition);
                        info!("Processed: {}#{}", name, frag);
                    }
                    Err(e) => {
                        debug!("Failed: {}#{}\n{}", &doc.id, frag, e);
                    }
                }
            }
        } else {
            panic!("Should not have empty definition");
        }
    }
    let code = scope.to_string();

    //println!("root:{}\ncode: {:?}", &root.display(), &code);

    // this is the root for output files
    let mut output = PathBuf::from("src/lexicon/");

    // TODO: change this to a match statement
    if let Ok(path) = root.strip_prefix("lexicons/") {
        output.push(path);
    } else {
        println!("ERROR 23749283");
    }

    // create missing directories
    let binding = output.with_extension("rs");
    let file_path = binding.as_path();
    if let Some(dir_path) = file_path.parent() {
        create_dir_all(dir_path).expect("directory creation");
    };

    // create the .rs
    let mut file = File::create(file_path).expect("cannot overwrite existing files");
    let _ = file.write(code.as_bytes());

    Ok(())
}

fn generate(scope: &mut Scope, name: &str, definition: LexiconType) -> String {
    match definition {
        LexiconType::Query(query) => query.to_rust(scope, name),
        LexiconType::Procedure(procedure) => procedure.to_rust(scope, name),
        LexiconType::Object(object) => object.to_rust(scope, name),
        LexiconType::Token(token) => token.to_rust(scope, name),
        LexiconType::String(string) => string.to_rust(scope, name),
        LexiconType::Record(record) => record.to_rust(scope, name),
        LexiconType::Subscription(subscription) => subscription.to_rust(scope, name),
        _ => unreachable!(),
    }
}
