pub type Record = com.atproto.repo.strongRef;
#[derive(Debug, Deserialize, Serialize)]
pub struct Record {
    record: pub type Record = com.atproto.repo.strongRef;,
}

pub type ViewBlocked = app.bsky.feed.defs#blockedAuthor;
#[derive(Debug, Deserialize, Serialize)]
pub struct ViewBlocked {
    uri: String,
    author: pub type Record = com.atproto.repo.strongRef;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct Record {
        record: pub type Record = com.atproto.repo.strongRef;,
    }

    pub type ViewBlocked = app.bsky.feed.defs#blockedAuthor;,
    blocked: bool,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ViewNotFound {
    uri: String,
    notFound: bool,
}