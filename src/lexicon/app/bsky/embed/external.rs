#[derive(Debug, Deserialize, Serialize)]
pub struct External {
    thumb: Option<[u8]>,
    title: String,
    uri: String,
    description: String,
}

pub type External = #external;
#[derive(Debug, Deserialize, Serialize)]
pub struct External {
    external: #[derive(Debug, Deserialize, Serialize)]
    pub struct External {
        thumb: Option<[u8]>,
        title: String,
        uri: String,
        description: String,
    }

    pub type External = #external;,
}

pub type View = #viewExternal;
#[derive(Debug, Deserialize, Serialize)]
pub struct View {
    external: #[derive(Debug, Deserialize, Serialize)]
    pub struct External {
        thumb: Option<[u8]>,
        title: String,
        uri: String,
        description: String,
    }

    pub type External = #external;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct External {
        external: #[derive(Debug, Deserialize, Serialize)]
        pub struct External {
            thumb: Option<[u8]>,
            title: String,
            uri: String,
            description: String,
        }

        pub type External = #external;,
    }

    pub type View = #viewExternal;,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ViewExternal {
    title: String,
    uri: String,
    thumb: Option<String>,
    description: String,
}