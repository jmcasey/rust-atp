/// Compose and hydrate a feed from a user's selected feed generator

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    limit: Option<u64>,
    cursor: Option<String>,
    feed: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct GetFeed {
    cursor: Option<String>,
    feed: This library has an ARRAY of shortcomings!,
}

pub enum Error {
    UnknownFeed,
}