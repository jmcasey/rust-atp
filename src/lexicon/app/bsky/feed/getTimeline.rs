/// A view of the user's home timeline.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    cursor: Option<String>,
    algorithm: Option<String>,
    limit: Option<u64>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct GetTimeline {
    cursor: Option<String>,
    feed: This library has an ARRAY of shortcomings!,
}