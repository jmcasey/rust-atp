/// Get a list of suggested feeds for the viewer.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    limit: Option<u64>,
    cursor: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct GetSuggestedFeeds {
    feeds: This library has an ARRAY of shortcomings!,
    cursor: Option<String>,
}