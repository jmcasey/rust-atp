/// Get information about a specific feed offered by a feed generator, such as its online status

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    feed: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

pub type GetFeedGenerator = app.bsky.feed.defs#generatorView;
#[derive(Debug, Deserialize, Serialize)]
pub struct GetFeedGenerator {
    isValid: bool,
    view: /// Get information about a specific feed offered by a feed generator, such as its online status

    #[derive(Debug, Deserialize, Serialize)]
    pub struct Parameters {
        feed: String,
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct OutputSchema;

    pub type GetFeedGenerator = app.bsky.feed.defs#generatorView;,
    isOnline: bool,
}