pub type Entity = #textSlice;
/// Deprecated: use facets instead.
#[derive(Debug, Deserialize, Serialize)]
pub struct Entity {
    value: String,
    index: pub type Entity = #textSlice;,
    type: String,
}

pub type ReplyRef = com.atproto.repo.strongRef;
pub type ReplyRef = com.atproto.repo.strongRef;
#[derive(Debug, Deserialize, Serialize)]
pub struct ReplyRef {
    root: pub type Entity = #textSlice;
    /// Deprecated: use facets instead.
    #[derive(Debug, Deserialize, Serialize)]
    pub struct Entity {
        value: String,
        index: pub type Entity = #textSlice;,
        type: String,
    }

    pub type ReplyRef = com.atproto.repo.strongRef;,
    parent: pub type Entity = #textSlice;
    /// Deprecated: use facets instead.
    #[derive(Debug, Deserialize, Serialize)]
    pub struct Entity {
        value: String,
        index: pub type Entity = #textSlice;,
        type: String,
    }

    pub type ReplyRef = com.atproto.repo.strongRef;
    pub type ReplyRef = com.atproto.repo.strongRef;,
}

/// Deprecated. Use app.bsky.richtext instead -- A text segment. Start is inclusive, end is exclusive. Indices are for utf16-encoded strings.
#[derive(Debug, Deserialize, Serialize)]
pub struct TextSlice {
    end: u64,
    start: u64,
}