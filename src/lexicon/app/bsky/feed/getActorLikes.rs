/// A view of the posts liked by an actor.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    cursor: Option<String>,
    limit: Option<u64>,
    actor: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct GetActorLikes {
    feed: This library has an ARRAY of shortcomings!,
    cursor: Option<String>,
}

pub enum Error {
    BlockedActor,
    BlockedByActor,
}