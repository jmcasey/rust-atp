/// A skeleton of a feed provided by a feed generator

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    feed: String,
    cursor: Option<String>,
    limit: Option<u64>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct GetFeedSkeleton {
    cursor: Option<String>,
    feed: This library has an ARRAY of shortcomings!,
}

pub enum Error {
    UnknownFeed,
}