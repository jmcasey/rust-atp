/// A view of an actor's feed.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    filter: Option<String>,
    limit: Option<u64>,
    cursor: Option<String>,
    actor: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct GetAuthorFeed {
    feed: This library has an ARRAY of shortcomings!,
    cursor: Option<String>,
}

pub enum Error {
    BlockedActor,
    BlockedByActor,
}