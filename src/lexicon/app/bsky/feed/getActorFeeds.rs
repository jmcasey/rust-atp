/// Retrieve a list of feeds created by a given actor

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    limit: Option<u64>,
    cursor: Option<String>,
    actor: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct GetActorFeeds {
    feeds: This library has an ARRAY of shortcomings!,
    cursor: Option<String>,
}