/// Register for push notifications with a service

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct RegisterPush {
    appId: String,
    platform: String,
    token: String,
    serviceDid: String,
}