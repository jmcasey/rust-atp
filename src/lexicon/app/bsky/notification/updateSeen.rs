/// Notify server that the user has seen notifications.

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct UpdateSeen {
    seenAt: String,
}