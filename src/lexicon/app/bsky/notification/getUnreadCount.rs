#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    seenAt: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct GetUnreadCount {
    count: u64,
}