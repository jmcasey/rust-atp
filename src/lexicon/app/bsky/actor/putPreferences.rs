/// Sets the private preferences attached to the account.

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

pub type PutPreferences = app.bsky.actor.defs#preferences;
#[derive(Debug, Deserialize, Serialize)]
pub struct PutPreferences {
    preferences: /// Sets the private preferences attached to the account.

    #[derive(Debug, Deserialize, Serialize)]
    pub struct InputSchema;

    pub type PutPreferences = app.bsky.actor.defs#preferences;,
}