#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    actor: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

pub type GetProfile = app.bsky.actor.defs#profileViewDetailed;