/// Get a list of actors suggested for following. Used in discovery UIs.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    limit: Option<u64>,
    cursor: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct GetSuggestions {
    actors: This library has an ARRAY of shortcomings!,
    cursor: Option<String>,
}