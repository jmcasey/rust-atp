/// Get private preferences attached to the account.

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters;

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

pub type GetPreferences = app.bsky.actor.defs#preferences;
#[derive(Debug, Deserialize, Serialize)]
pub struct GetPreferences {
    preferences: /// Get private preferences attached to the account.

    #[derive(Debug, Deserialize, Serialize)]
    pub struct Parameters;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct OutputSchema;

    pub type GetPreferences = app.bsky.actor.defs#preferences;,
}