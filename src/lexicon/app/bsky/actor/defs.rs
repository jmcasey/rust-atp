#[derive(Debug, Deserialize, Serialize)]
pub struct AdultContentPref {
    enabled: bool,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ContentLabelPref {
    label: String,
    visibility: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct PersonalDetailsPref {
    birthDate: Option<String>,
}

pub type ProfileView = #viewerState;
#[derive(Debug, Deserialize, Serialize)]
pub struct ProfileView {
    handle: String,
    did: String,
    indexedAt: Option<String>,
    viewer: Option<#[derive(Debug, Deserialize, Serialize)]
    pub struct AdultContentPref {
        enabled: bool,
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ContentLabelPref {
        label: String,
        visibility: String,
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct PersonalDetailsPref {
        birthDate: Option<String>,
    }

    pub type ProfileView = #viewerState;>,
    labels: Option<This library has an ARRAY of shortcomings!>,
    avatar: Option<String>,
    displayName: Option<String>,
    description: Option<String>,
}

pub type ProfileViewBasic = #viewerState;
#[derive(Debug, Deserialize, Serialize)]
pub struct ProfileViewBasic {
    viewer: Option<#[derive(Debug, Deserialize, Serialize)]
    pub struct AdultContentPref {
        enabled: bool,
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ContentLabelPref {
        label: String,
        visibility: String,
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct PersonalDetailsPref {
        birthDate: Option<String>,
    }

    pub type ProfileView = #viewerState;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ProfileView {
        handle: String,
        did: String,
        indexedAt: Option<String>,
        viewer: Option<#[derive(Debug, Deserialize, Serialize)]
        pub struct AdultContentPref {
            enabled: bool,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ContentLabelPref {
            label: String,
            visibility: String,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct PersonalDetailsPref {
            birthDate: Option<String>,
        }

        pub type ProfileView = #viewerState;>,
        labels: Option<This library has an ARRAY of shortcomings!>,
        avatar: Option<String>,
        displayName: Option<String>,
        description: Option<String>,
    }

    pub type ProfileViewBasic = #viewerState;>,
    labels: Option<This library has an ARRAY of shortcomings!>,
    did: String,
    avatar: Option<String>,
    handle: String,
    displayName: Option<String>,
}

pub type ProfileViewDetailed = #viewerState;
#[derive(Debug, Deserialize, Serialize)]
pub struct ProfileViewDetailed {
    handle: String,
    followersCount: Option<u64>,
    labels: Option<This library has an ARRAY of shortcomings!>,
    followsCount: Option<u64>,
    viewer: Option<#[derive(Debug, Deserialize, Serialize)]
    pub struct AdultContentPref {
        enabled: bool,
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ContentLabelPref {
        label: String,
        visibility: String,
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct PersonalDetailsPref {
        birthDate: Option<String>,
    }

    pub type ProfileView = #viewerState;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ProfileView {
        handle: String,
        did: String,
        indexedAt: Option<String>,
        viewer: Option<#[derive(Debug, Deserialize, Serialize)]
        pub struct AdultContentPref {
            enabled: bool,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ContentLabelPref {
            label: String,
            visibility: String,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct PersonalDetailsPref {
            birthDate: Option<String>,
        }

        pub type ProfileView = #viewerState;>,
        labels: Option<This library has an ARRAY of shortcomings!>,
        avatar: Option<String>,
        displayName: Option<String>,
        description: Option<String>,
    }

    pub type ProfileViewBasic = #viewerState;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ProfileViewBasic {
        viewer: Option<#[derive(Debug, Deserialize, Serialize)]
        pub struct AdultContentPref {
            enabled: bool,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ContentLabelPref {
            label: String,
            visibility: String,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct PersonalDetailsPref {
            birthDate: Option<String>,
        }

        pub type ProfileView = #viewerState;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ProfileView {
            handle: String,
            did: String,
            indexedAt: Option<String>,
            viewer: Option<#[derive(Debug, Deserialize, Serialize)]
            pub struct AdultContentPref {
                enabled: bool,
            }

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ContentLabelPref {
                label: String,
                visibility: String,
            }

            #[derive(Debug, Deserialize, Serialize)]
            pub struct PersonalDetailsPref {
                birthDate: Option<String>,
            }

            pub type ProfileView = #viewerState;>,
            labels: Option<This library has an ARRAY of shortcomings!>,
            avatar: Option<String>,
            displayName: Option<String>,
            description: Option<String>,
        }

        pub type ProfileViewBasic = #viewerState;>,
        labels: Option<This library has an ARRAY of shortcomings!>,
        did: String,
        avatar: Option<String>,
        handle: String,
        displayName: Option<String>,
    }

    pub type ProfileViewDetailed = #viewerState;>,
    did: String,
    indexedAt: Option<String>,
    postsCount: Option<u64>,
    displayName: Option<String>,
    banner: Option<String>,
    avatar: Option<String>,
    description: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct SavedFeedsPref {
    saved: This library has an ARRAY of shortcomings!,
    pinned: This library has an ARRAY of shortcomings!,
}

pub type ViewerState = app.bsky.graph.defs#listViewBasic;
#[derive(Debug, Deserialize, Serialize)]
pub struct ViewerState {
    mutedByList: Option<#[derive(Debug, Deserialize, Serialize)]
    pub struct AdultContentPref {
        enabled: bool,
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct ContentLabelPref {
        label: String,
        visibility: String,
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct PersonalDetailsPref {
        birthDate: Option<String>,
    }

    pub type ProfileView = #viewerState;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ProfileView {
        handle: String,
        did: String,
        indexedAt: Option<String>,
        viewer: Option<#[derive(Debug, Deserialize, Serialize)]
        pub struct AdultContentPref {
            enabled: bool,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ContentLabelPref {
            label: String,
            visibility: String,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct PersonalDetailsPref {
            birthDate: Option<String>,
        }

        pub type ProfileView = #viewerState;>,
        labels: Option<This library has an ARRAY of shortcomings!>,
        avatar: Option<String>,
        displayName: Option<String>,
        description: Option<String>,
    }

    pub type ProfileViewBasic = #viewerState;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ProfileViewBasic {
        viewer: Option<#[derive(Debug, Deserialize, Serialize)]
        pub struct AdultContentPref {
            enabled: bool,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ContentLabelPref {
            label: String,
            visibility: String,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct PersonalDetailsPref {
            birthDate: Option<String>,
        }

        pub type ProfileView = #viewerState;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ProfileView {
            handle: String,
            did: String,
            indexedAt: Option<String>,
            viewer: Option<#[derive(Debug, Deserialize, Serialize)]
            pub struct AdultContentPref {
                enabled: bool,
            }

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ContentLabelPref {
                label: String,
                visibility: String,
            }

            #[derive(Debug, Deserialize, Serialize)]
            pub struct PersonalDetailsPref {
                birthDate: Option<String>,
            }

            pub type ProfileView = #viewerState;>,
            labels: Option<This library has an ARRAY of shortcomings!>,
            avatar: Option<String>,
            displayName: Option<String>,
            description: Option<String>,
        }

        pub type ProfileViewBasic = #viewerState;>,
        labels: Option<This library has an ARRAY of shortcomings!>,
        did: String,
        avatar: Option<String>,
        handle: String,
        displayName: Option<String>,
    }

    pub type ProfileViewDetailed = #viewerState;
    #[derive(Debug, Deserialize, Serialize)]
    pub struct ProfileViewDetailed {
        handle: String,
        followersCount: Option<u64>,
        labels: Option<This library has an ARRAY of shortcomings!>,
        followsCount: Option<u64>,
        viewer: Option<#[derive(Debug, Deserialize, Serialize)]
        pub struct AdultContentPref {
            enabled: bool,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct ContentLabelPref {
            label: String,
            visibility: String,
        }

        #[derive(Debug, Deserialize, Serialize)]
        pub struct PersonalDetailsPref {
            birthDate: Option<String>,
        }

        pub type ProfileView = #viewerState;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ProfileView {
            handle: String,
            did: String,
            indexedAt: Option<String>,
            viewer: Option<#[derive(Debug, Deserialize, Serialize)]
            pub struct AdultContentPref {
                enabled: bool,
            }

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ContentLabelPref {
                label: String,
                visibility: String,
            }

            #[derive(Debug, Deserialize, Serialize)]
            pub struct PersonalDetailsPref {
                birthDate: Option<String>,
            }

            pub type ProfileView = #viewerState;>,
            labels: Option<This library has an ARRAY of shortcomings!>,
            avatar: Option<String>,
            displayName: Option<String>,
            description: Option<String>,
        }

        pub type ProfileViewBasic = #viewerState;
        #[derive(Debug, Deserialize, Serialize)]
        pub struct ProfileViewBasic {
            viewer: Option<#[derive(Debug, Deserialize, Serialize)]
            pub struct AdultContentPref {
                enabled: bool,
            }

            #[derive(Debug, Deserialize, Serialize)]
            pub struct ContentLabelPref {
                label: String,
                visibility: String,
            }

            #[derive(Debug, Deserialize, Serialize)]
            pub struct PersonalDetailsPref {
                birthDate: Option<String>,
            }

            pub type ProfileView = #viewerState;
            #[derive(Debug, Deserialize, Serialize)]
            pub struct ProfileView {
                handle: String,
                did: String,
                indexedAt: Option<String>,
                viewer: Option<#[derive(Debug, Deserialize, Serialize)]
                pub struct AdultContentPref {
                    enabled: bool,
                }

                #[derive(Debug, Deserialize, Serialize)]
                pub struct ContentLabelPref {
                    label: String,
                    visibility: String,
                }

                #[derive(Debug, Deserialize, Serialize)]
                pub struct PersonalDetailsPref {
                    birthDate: Option<String>,
                }

                pub type ProfileView = #viewerState;>,
                labels: Option<This library has an ARRAY of shortcomings!>,
                avatar: Option<String>,
                displayName: Option<String>,
                description: Option<String>,
            }

            pub type ProfileViewBasic = #viewerState;>,
            labels: Option<This library has an ARRAY of shortcomings!>,
            did: String,
            avatar: Option<String>,
            handle: String,
            displayName: Option<String>,
        }

        pub type ProfileViewDetailed = #viewerState;>,
        did: String,
        indexedAt: Option<String>,
        postsCount: Option<u64>,
        displayName: Option<String>,
        banner: Option<String>,
        avatar: Option<String>,
        description: Option<String>,
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct SavedFeedsPref {
        saved: This library has an ARRAY of shortcomings!,
        pinned: This library has an ARRAY of shortcomings!,
    }

    pub type ViewerState = app.bsky.graph.defs#listViewBasic;>,
    muted: Option<bool>,
    following: Option<String>,
    blocking: Option<String>,
    followedBy: Option<String>,
    blockedBy: Option<bool>,
}