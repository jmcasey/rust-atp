/// A text segment. Start is inclusive, end is exclusive. Indices are for utf8-encoded strings.
#[derive(Debug, Deserialize, Serialize)]
pub struct ByteSlice {
    byteStart: u64,
    byteEnd: u64,
}

/// A facet feature for links.
#[derive(Debug, Deserialize, Serialize)]
pub struct Link {
    uri: String,
}

/// A facet feature for actor mentions.
#[derive(Debug, Deserialize, Serialize)]
pub struct Mention {
    did: String,
}