/// Fetch a list of lists that belong to an actor

#[derive(Debug, Deserialize, Serialize)]
pub struct Parameters {
    cursor: Option<String>,
    limit: Option<u64>,
    actor: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct GetLists {
    lists: This library has an ARRAY of shortcomings!,
    cursor: Option<String>,
}