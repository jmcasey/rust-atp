pub struct Record;

#[derive(Debug, Deserialize, Serialize)]
pub struct Listblock {
    createdAt: String,
    subject: String,
}