pub struct Record;

#[derive(Debug, Deserialize, Serialize)]
pub struct Follow {
    subject: String,
    createdAt: String,
}