pub struct Record;

#[derive(Debug, Deserialize, Serialize)]
pub struct Block {
    subject: String,
    createdAt: String,
}