/// Mute an actor by did or handle.

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct MuteActor {
    actor: String,
}