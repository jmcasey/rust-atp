/// Unmute a list of actors.

#[derive(Debug, Deserialize, Serialize)]
pub struct InputSchema;

#[derive(Debug, Deserialize, Serialize)]
pub struct UnmuteActorList {
    list: String,
}