pub struct Record;

#[derive(Debug, Deserialize, Serialize)]
pub struct Listitem {
    createdAt: String,
    list: String,
    subject: String,
}