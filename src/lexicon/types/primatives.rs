use codegen::Scope;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(tag = "type")]
pub enum LexPrimitive {
    #[serde(rename = "null")]
    Null,
    #[serde(rename = "boolean")]
    Bool(LexBoolean),
    #[serde(rename = "integer")]
    Integer(LexInteger),
    #[serde(rename = "string")]
    String(LexString),
    #[serde(rename = "bytes")]
    Bytes(LexBytes),
    #[serde(rename = "cid-link")]
    Link(LexCidLink),
    #[serde(rename = "blob")]
    Blob(LexBlob),
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct LexBoolean {
    description: Option<String>,
    default: Option<bool>,
    r#const: Option<bool>,
}

impl LexBoolean {
    // this will be incomplete for a while
    // would like to add .validate() method to enforce reqs
    pub fn to_rust(&self, _scope: &mut Scope, _name: &str) -> String {
        String::from("bool")
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct LexInteger {
    description: Option<String>,
    default: Option<i64>,
    minimum: Option<i64>,
    maximum: Option<i64>,
    r#enum: Option<Vec<i64>>,
    r#const: Option<i64>,
}

impl LexInteger {
    // this will be incomplete for a while
    // would like to add .validate() method to enforce reqs
    pub fn to_rust(&self, _scope: &mut Scope, _name: &str) -> String {
        String::from("u64")
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct LexString {
    description: Option<String>,
    // value: String?
    default: Option<String>,
    format: Option<String>,
    min_length: Option<i64>, // length in UTF-8 bytes
    max_length: Option<i64>, // length in UTF-8 bytes
    min_graphemes: Option<i64>,
    max_graphemes: Option<i64>,
    r#enum: Option<Vec<String>>,
    r#const: Option<String>,
    known_values: Option<Vec<String>>,
}

impl LexString {
    // this will be incomplete for a while
    // would like to add .validate() method to enforce reqs
    pub fn to_rust(&self, _scope: &mut Scope, _name: &str) -> String {
        String::from("String")
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct LexBytes {
    description: Option<String>,
    min_length: Option<i64>,
    max_length: Option<i64>,
}

impl LexBytes {
    // this will be incomplete for a while
    // would like to add .validate() method to enforce reqs
    pub fn to_rust(&self, _scope: &mut Scope, _name: &str) -> String {
        String::from("[u8]")
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct LexCidLink {
    description: Option<String>,
}

impl LexCidLink {
    // this will be incomplete for a while
    // would like to add .validate() method to enforce reqs
    pub fn to_rust(&self, _scope: &mut Scope, _name: &str) -> String {
        String::from("String")
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct LexBlob {
    description: Option<String>,
    accept: Option<Vec<String>>, // MIME
    max_size: Option<i64>,       //bytes
}

impl LexBlob {
    // this will be incomplete for a while
    // would like to add .validate() method to enforce reqs
    pub fn to_rust(&self, _scope: &mut Scope, _name: &str) -> String {
        String::from("[u8]")
    }
}
