use crate::lexicon::core::ToRust;
use crate::lexicon::types::meta::r#ref::LexRef;
use crate::lexicon::core::LexiconType;

use crate::lexicon::types::primatives::*;
use codegen::Scope;
use log::info;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct LexArray {
    description: Option<String>,
    items: LexArrayItem,
    min_length: Option<u64>,
    max_length: Option<u64>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "type", rename_all = "kebab-case")]
pub enum LexArrayItem {
    Bool(LexBoolean),
    Integer(LexInteger),
    String(LexString),
    Unknown(LexUnknown),
    Bytes(LexBytes),
    #[serde(rename = "cid-link")]
    Link(LexCidLink),
    Blob(LexBlob),
    Ref(LexRef),
    Union(LexUnion),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "type", rename_all = "kebab-case")]
pub enum LexSchemaItem {
    Object(LexObject),
    Ref(LexRef),
    Union(LexUnion),
}

impl LexSchemaItem {
    pub fn to_rust(&self, scope: &mut Scope, name: &str) -> String {
        match &self {
            LexSchemaItem::Object(o) => o.to_rust(scope, name),
            LexSchemaItem::Ref(r) => r.to_rust(scope, name),
            LexSchemaItem::Union(u) => u.to_rust(scope, name),
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct LexObject {
    pub required: Option<Vec<String>>,
    pub properties: LexMap,
    pub description: Option<String>,
    pub nullable: Option<Vec<String>>,
}

impl LexObject {
    pub fn to_rust(&self, scope: &mut Scope, name: &str) -> String {
        info!("Generating object: {}", name);

        // required
        let required_fields = &self.required;

        let mut field_types = HashMap::new();
        for (prop_name, prop_value) in &self.properties {
            let prop_type = prop_value.to_rust(scope, name);

            // If the property is required, wrap it in an Option
            let final_type = if required_fields
                .as_ref()
                .map_or(false, |vec| vec.contains(prop_name))
            {
                prop_type
            } else {
                format!("Option<{}>", prop_type)
            };

            field_types.insert(prop_name.clone(), final_type);
        }

        let object = scope
            .new_struct(name)
            .vis("pub")
            .derive("Debug, Deserialize, Serialize");

        if let Some(description) = &self.description {
            object.doc(description);
        };

        for (field_name, field_type) in field_types {
            object.field(&field_name, field_type);
        }

        if let Some(_nullable_fields) = &self.nullable {
            println!("### Look! Nullables!");
        };

        scope.to_string()
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct LexParams {
    pub required: Option<Vec<String>>,
    pub description: Option<String>,
    pub properties: LexMap,
}

impl LexParams {
    pub fn to_rust(&self, scope: &mut Scope, name: &str) -> String {
        info!("Generating params for {}", name);

        let required_fields = &self.required;

        let mut field_types = HashMap::new();
        for (prop_name, prop_value) in &self.properties {
            let prop_type = prop_value.to_rust(scope, name);

            // If the property is required, wrap it in an Option
            let final_type = if required_fields
                .as_ref()
                .map_or(false, |vec| vec.contains(prop_name))
            {
                prop_type
            } else {
                format!("Option<{}>", prop_type)
            };

            field_types.insert(prop_name.clone(), final_type);
        }

        let object = scope
            .new_struct("Parameters")
            .vis("pub")
            .derive("Debug, Deserialize, Serialize");

        if let Some(description) = &self.description {
            object.doc(description);
        }

        for (field_name, field_type) in field_types {
            object.field(&field_name, field_type);
        }

        scope.to_string()
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub enum LexParamsProperties {
    #[serde(rename = "boolean")]
    Bool(LexBoolean),
    #[serde(rename = "integer")]
    Integer(LexInteger),
    #[serde(rename = "string")]
    String(LexString),
    #[serde(rename = "unknown")]
    Unknown(LexUnknown),
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct LexMap(HashMap<String, LexiconType>);

impl<'a> IntoIterator for &'a LexMap {
    type Item = (&'a String, &'a LexiconType);
    type IntoIter = std::collections::hash_map::Iter<'a, String, LexiconType>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.iter()
    }
}
