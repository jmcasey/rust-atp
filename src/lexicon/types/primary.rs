use crate::lexicon::types::containers::*;
use codegen::Enum;
use codegen::Scope;

use log::info;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct LexXrpcQuery {
    pub description: Option<String>,
    pub parameters: Option<LexParams>,
    pub output: Option<Box<LexXrpcBody>>,
    pub errors: Option<Vec<LexXrpcError>>,
}

impl LexXrpcQuery {
    pub fn to_rust(&self, scope: &mut Scope, name: &str) -> String {
        info!("Generating query: {}", name);

        if let Some(description) = &self.description {
            scope.raw(format!("/// {}", description));
        };

        if let Some(params) = &self.parameters {
            params.to_rust(scope, name);
        };

        if let Some(output) = &self.output {
            let _output_schema = scope
                .new_struct("OutputSchema")
                .vis("pub")
                .derive("Debug, Deserialize, Serialize");

            output.to_rust(scope, name);
        }

        // errors
        if let Some(errors) = &self.errors {
            let error = scope.new_enum("Error").vis("pub");

            for each in errors {
                each.to_rust(error, name);
            }
        };

        scope.to_string()
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct LexXrpcProcedure {
    pub description: Option<String>,
    pub parameters: Option<Box<LexParams>>,
    pub input: Option<Box<LexXrpcBody>>,
    pub output: Option<Box<LexXrpcBody>>,
    pub errors: Option<Vec<LexXrpcError>>,
}

impl LexXrpcProcedure {
    pub fn to_rust(&self, scope: &mut Scope, name: &str) -> String {
        info!("Generating procedure: {}", name);

        if let Some(description) = &self.description {
            scope.raw(format!("/// {}", description));
        };

        if let Some(input) = &self.input {
            let _input_schema = scope
                .new_struct("InputSchema")
                .vis("pub")
                .derive("Debug, Deserialize, Serialize");

            input.to_rust(scope, name);
        }

        if let Some(params) = &self.parameters {
            params.to_rust(scope, name);
        };

        if let Some(output) = &self.output {
            let _output_schema = scope
                .new_struct("OutputSchema")
                .vis("pub")
                .derive("Debug, Deserialize, Serialize");

            output.to_rust(scope, name);
        }

        // errors
        if let Some(errors) = &self.errors {
            let error = scope.new_enum("Error").vis("pub");

            for each in errors {
                each.to_rust(error, name);
            }
        };

        scope.to_string()
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct LexXrpcSubscription {
    pub description: Option<String>,
    pub parameters: Option<Box<LexParams>>,
    pub message: Option<Box<LexXrpcMessage>>,
    pub errors: Option<Vec<LexXrpcError>>,
}

impl LexXrpcSubscription {
    pub fn to_rust(&self, scope: &mut Scope, name: &str) -> String {
        info!("Generating subscription: {}", name);

        if let Some(description) = &self.description {
            scope.raw(format!("/// {}", description));
        };

        if let Some(message) = &self.message {
            let _input_schema = scope
                .new_struct("MessageSchema")
                .vis("pub")
                .derive("Debug, Deserialize, Serialize");

            message.to_rust(scope, name);
        }

        if let Some(params) = &self.parameters {
            params.to_rust(scope, name);
        };

        // errors
        if let Some(errors) = &self.errors {
            let error = scope.new_enum("Error").vis("pub");

            for each in errors {
                each.to_rust(error, name);
            }
        };

        scope.to_string()
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct LexXrpcRecord {
    pub description: Option<String>,
    pub key: String,
    pub record: LexRecord,
    pub errors: Option<Vec<LexXrpcError>>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct LexXrpcMessage {
    pub description: Option<String>,
    pub schema: LexSchemaItem, // Union of refs
}

impl LexXrpcMessage {
    pub fn to_rust(&self, scope: &mut Scope, name: &str) -> String {
        info!("Generating query: {}", name);

        if let Some(description) = &self.description {
            scope.raw(format!("/// {}", description));
        };

        let _ = &self.schema.to_rust(scope, name);

        scope.to_string()
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct LexXrpcBody {
    pub description: Option<String>,
    pub encoding: String, //mime
    pub schema: Option<LexSchemaItem>,
}

impl LexXrpcBody {
    pub fn to_rust(&self, scope: &mut Scope, name: &str) -> String {
        if let Some(description) = &self.description {
            scope.raw(format!("/// {}", description));
        };

        for each in &self.schema {
            each.to_rust(scope, name);
        }

        scope.to_string()
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct LexXrpcError {
    pub name: String,
    pub description: Option<String>,
}

impl LexXrpcError {
    pub fn to_rust<'a>(&'a self, errors: &'a mut Enum, _name: &str) -> &mut Enum {
        let _ = errors.new_variant(&self.name);
        if let Some(description) = &self.description {
            errors.doc(description);
        }
        errors
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct LexRecord {
    pub key: Option<String>,
    pub record: LexObject,
}

impl LexRecord {
    pub fn to_rust(&self, scope: &mut Scope, name: &str) -> String {
        info!("Generating record: {}", name);

        let _record = scope.new_struct("Record").vis("pub");

        let _ = &self.record.to_rust(scope, name);

        scope.to_string()
    }
}
