use super::did::operations::ValidatedSignedOperation;
use anyhow::ensure;
use anyhow::Error;
use chrono::Utc;
use libipld::cid::multibase::Base;
use serde::{Deserialize, Serialize};
use serde_json::json;

pub mod operations;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct DidDocData {
    did: String,
    #[serde(rename = "signingKey")]
    signing_key: String,
    #[serde(rename = "recoveryKey")]
    recovery_key: String,
    #[serde(rename = "username")]
    handle: String,
    #[serde(rename = "atpPds")]
    service: String,
}

impl DidDocData {
    pub fn generate_document(self) -> serde_json::Value {
        // TODO: refactor this into the parent Struct
        let did = &self.did;
        let did_signing_key = format!("{}#signingKey)", &did);
        let did_recovery_key = format!("{}#recoveryKey)", &did);
        let did_service = format!("{}#atpPds)", &did);

        json!({
            "@context": [
                "https://www.w3.org/ns/did/v1",
                "https://w3id.org/security/suites/ecdsa-2019/v1",
            ],
            "id": &did,
            "alsoKnownAs": [ &self.service ],
            "verificationMethod": [
                {
                "id": &did_signing_key,
                "type": "EcdsaSecp256r1VerificationKey2019",
                "controller": &did,
                "publicKeyMultibase": &self.signing_key[8..],
                },
                {
                "id": &did_recovery_key,
                "type": "EcdsaSecp256r1VerificationKey2019",
                "controller": &did,
                "publicKeyMultibase": &self.recovery_key[8..],
                }
            ],
            "assertionMethod": [ &did_signing_key ],
            "capabilityInvocation": [ &did_signing_key ],
            "capabilityDelegation": [ &did_signing_key ],
            "service": [
                {
                "id": &did_service,
                "type": "AtpPersonalDataServer",
                "serviceEndpoint": &self.service,
                }
            ]
        })
    }

    pub fn validate_operation(self, op: &ValidatedSignedOperation) -> Result<(), Error> {
        // TODO: this
        ensure!(
            op.did() == op.operation().did(),
            "invalid operation: did does not match"
        );
        ensure!(
            op.operation().verify().unwrap() == (),
            "invalid operation: cryptographic signature is invalid"
        );
        ensure!(
            op.cid()
                == op
                    .operation()
                    .as_block()
                    .unwrap()
                    .cid()
                    .to_string_of_base(Base::Base16Lower)
                    .unwrap(),
            "invalid operation: incorrect cid"
        );
        ensure!(
            !op.nullified(),
            "invalid operation: operation is marked as nullified"
        );
        ensure!(
            op.created_at() < Utc::now().timestamp_micros(),
            "invalid operation: cannot execute operation created in the future"
        );
        Ok(())
    }

    pub fn execute(self, op: &ValidatedSignedOperation) -> Option<Self> {
        // match self.validate_operation(op);
        let did_doc_data = match op.operation().r#type().as_str() {
            "create" => DidDocData::create(op), /*
            "update_username" => update_username(self),
            "update_atp_pds" => update_atp_pds(self),
            "rotate_signing_key" => rotate_signing_key(self),
            "rotate_recovery_key" => rotate_recovery_key(self),*/
            _ => unimplemented!(),
        };
        did_doc_data
    }

    pub fn create(op: &ValidatedSignedOperation) -> Option<Self> {
        // TODO: verify valid operation
        if op.operation().r#type() != "create" {
            // This method only makes sense when executed against the genesis
            // operation. Potential TODO: Recurse back to genesis operation.
            None
        } else {
            let did_doc_data = DidDocData {
                did: op.did(),
                signing_key: op.signing_key().unwrap(),
                recovery_key: op.recovery_key().unwrap(),
                handle: op.username().unwrap(),
                service: op.atp_pds().unwrap(),
            };
            Some(did_doc_data)
        }
    }

    pub fn update_username(self, op: ValidatedSignedOperation) -> Option<Self> {
        // TODO: verify valid operation
        if &op.operation().r#type() != "update_username" {
            // This method only makes sense when executed against the genesis
            // operation. Potential TODO: Recurse back to genesis operation.
            None
        } else {
            let did_doc_data = DidDocData {
                did: self.did.clone(),
                signing_key: self.signing_key.clone(),
                recovery_key: self.signing_key.clone(),
                handle: op.operation().username().unwrap(),
                service: self.service.clone(),
            };
            Some(did_doc_data)
        }
    }
    /*
    pub fn execute(self, op: ValidatedSignedOperation) -> Self {
        let diddocdata = match op.r#type {
            "create" => create(self),
            "update_username" => update_username(self),
            "update_atp_pds" => update_atp_pds(self),
            "rotate_signing_key" => rotate_signing_key(self),
            "rotate_recovery_key" => rotate_recovery_key(self),
        }
    }

    pub fn execute(self, op: ValidatedSignedOperation) -> Self {
        let diddocdata = match op.r#type {
            "create" => create(self),
            "update_username" => update_username(self),
            "update_atp_pds" => update_atp_pds(self),
            "rotate_signing_key" => rotate_signing_key(self),
            "rotate_recovery_key" => rotate_recovery_key(self),
        }
    }
    */
}
