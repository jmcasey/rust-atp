use chrono::prelude::*;
use data_encoding::BASE64URL_NOPAD;
use did_key::*;
use libipld::multibase::Base;
use libipld::multihash::Code;
use libipld::DagCbor;
use libipld::{Block, DefaultParams};
use libipld_cbor::DagCborCodec;
use serde::{self, Deserializer, Serializer};
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha256};

#[derive(Clone, DagCbor, Debug, Deserialize, Serialize)]
pub struct Operation {
    r#type: String,
    #[serde(rename = "signingKey")]
    signing_key: Option<String>,
    #[serde(rename = "recoveryKey")]
    recovery_key: Option<String>,
    prev: Option<String>,
    #[serde(rename = "username")]
    handle: Option<String>,
    service: Option<String>,
}

#[derive(Clone, DagCbor, Debug, Deserialize, Serialize)]
pub struct SignedOperation {
    r#type: String,
    #[serde(rename = "signingKey")]
    signing_key: Option<String>,
    #[serde(rename = "recoveryKey")]
    recovery_key: Option<String>,
    prev: Option<String>,
    #[serde(rename = "username")]
    handle: Option<String>,
    service: Option<String>,
    #[serde(serialize_with = "serialize_sig", deserialize_with = "deserialize_sig")]
    sig: Vec<u8>,
}

#[derive(Clone, DagCbor, Debug, Deserialize, Serialize)]
pub struct ValidatedSignedOperation {
    did: String,
    operation: SignedOperation,
    cid: String,
    #[serde(serialize_with = "serialize_bool")]
    #[serde(deserialize_with = "deserialize_bool")]
    nullified: bool,
    #[serde(rename = "createdAt")]
    created_at: i64, // count of microseconds since UNIX epoch
}

fn serialize_sig<S>(sig: &[u8], serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    let sig_str = BASE64URL_NOPAD.encode(sig);
    serializer.serialize_str(&sig_str)
}

fn deserialize_sig<'de, D>(deserializer: D) -> Result<Vec<u8>, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let sig_str = String::deserialize(deserializer)?;
    let sig = BASE64URL_NOPAD
        .decode(sig_str.as_bytes())
        .map_err(serde::de::Error::custom)?;
    Ok(sig)
}

// Serialize a boolean as a 1 or 0.
fn serialize_bool<S>(value: &bool, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    serializer.serialize_u8(*value as u8)
}

// Deserialize a boolean from a 1 or 0.
fn deserialize_bool<'de, D>(deserializer: D) -> Result<bool, D::Error>
where
    D: Deserializer<'de>,
{
    match u8::deserialize(deserializer)? {
        0 => Ok(false),
        1 => Ok(true),
        other => Err(serde::de::Error::invalid_value(
            serde::de::Unexpected::Unsigned(other as u64),
            &"zero or one",
        )),
    }
}

impl Operation {
    pub fn new() -> Self {
        Self {
            r#type: String::new(),
            signing_key: None,
            recovery_key: None,
            prev: None,
            handle: None,
            service: None,
        }
    }

    pub fn r#type(mut self, r#type: impl Into<String>) -> Self {
        self.r#type = r#type.into();
        self
    }

    pub fn signing_key(mut self, signing_key: impl Into<String>) -> Self {
        self.signing_key = Some(signing_key.into());
        self
    }

    pub fn recovery_key(mut self, recovery_key: impl Into<String>) -> Self {
        self.recovery_key = Some(recovery_key.into());
        self
    }

    pub fn prev(mut self, prev: String) -> Self {
        self.prev = Some(prev);
        self
    }

    pub fn handle(mut self, handle: impl Into<String>) -> Self {
        self.handle = Some(handle.into());
        self
    }

    pub fn service(mut self, service: impl Into<String>) -> Self {
        self.service = Some(service.into());
        self
    }

    pub fn build(self) -> Operation {
        Operation {
            r#type: self.r#type,
            signing_key: self.signing_key,
            recovery_key: self.recovery_key,
            prev: self.prev,
            handle: self.handle,
            service: self.service,
        }
    }

    pub fn sign(self, keypair: &PatchedKeyPair) -> SignedOperation {
        let block = Block::<DefaultParams>::encode(DagCborCodec, Code::Sha2_256, &self)
            .expect("encode DAG-CBOR");

        let sig = keypair.sign(block.data());

        SignedOperation {
            r#type: self.r#type,
            signing_key: self.signing_key,
            recovery_key: self.recovery_key,
            prev: self.prev,
            handle: self.handle,
            service: self.service,
            sig,
        }
    }
}

impl Default for Operation {
    fn default() -> Self {
        Self::new()
    }
}

impl SignedOperation {
    pub fn as_block(&self) -> Result<Block<DefaultParams>, did_key::Error> {
        let op = Operation {
            r#type: self.r#type.clone(),
            signing_key: self.signing_key.clone(),
            recovery_key: self.recovery_key.clone(),
            prev: self.prev.clone(),
            handle: self.handle.clone(),
            service: self.service.clone(),
        };

        let block = Block::<DefaultParams>::encode(DagCborCodec, Code::Sha2_256, &op)
            .expect("encode DAG-CBOR");
        Ok(block)
    }

    pub fn verify(self) -> Result<(), did_key::Error> {
        let sig = &self.sig.as_ref();
        let key_str = &self.signing_key.as_ref().unwrap();
        let key = did_key::resolve(key_str).unwrap();
        let block = self.as_block().unwrap();

        key.verify(block.data(), sig)
    }

    // https://atproto.com/specs/did-plc
    //
    // The DID itself is derived from the sha256 hash of the first operation in the
    // log. It is then base32 encoded and truncated to 24 chars.
    pub fn did(&self) -> String {
        if self.r#type != "create" {
            // This method only makes sense when executed against the genesis
            // operation. Potential TODO: Recurse back to genesis operation.
            "Not a genesis operation".to_string()
        } else {
            let block = Block::<DefaultParams>::encode(DagCborCodec, Code::Sha2_256, &self)
                .expect("encode DAG-CBOR");

            let hash = Sha256::digest(block.data());

            let b32 = data_encoding::BASE32_NOPAD.encode(&hash);

            format!("did:plc:{}", &b32[0..24])
        }
    }

    pub fn r#type(&self) -> String {
        self.r#type.clone()
    }

    pub fn signing_key(&self) -> Option<String> {
        self.signing_key.clone()
    }

    pub fn recovery_key(&self) -> Option<String> {
        self.recovery_key.clone()
    }

    pub fn username(&self) -> Option<String> {
        self.handle.clone()
    }

    pub fn atp_pds(&self) -> Option<String> {
        self.service.clone()
    }

    pub fn signature(&self) -> Vec<u8> {
        self.sig.clone()
    }

    pub fn validate(&self) -> Result<ValidatedSignedOperation, Error> {
        let block = Block::<DefaultParams>::encode(DagCborCodec, Code::Sha2_256, &self)
            .expect("encode DAG-CBOR");

        let valid_signed_op = ValidatedSignedOperation {
            did: self.did(),
            operation: self.clone(), // JSON?
            cid: block.cid().to_string_of_base(Base::Base16Lower).unwrap(),
            nullified: false,
            created_at: Utc::now().timestamp_micros(),
        };

        Ok(valid_signed_op)
    }
}

impl ValidatedSignedOperation {
    pub fn cid(&self) -> String {
        self.cid.clone()
    }

    pub fn did(&self) -> String {
        self.did.clone()
    }

    pub fn operation(&self) -> SignedOperation {
        self.operation.clone()
    }

    pub fn created_at(&self) -> i64 {
        self.created_at
    }

    pub fn nullified(&self) -> bool {
        self.nullified
    }

    pub fn signing_key(&self) -> Option<String> {
        self.operation.signing_key.clone()
    }

    pub fn recovery_key(&self) -> Option<String> {
        self.operation.recovery_key.clone()
    }

    pub fn username(&self) -> Option<String> {
        self.operation.handle.clone()
    }

    pub fn atp_pds(&self) -> Option<String> {
        self.operation.service.clone()
    }
}
