// ########################################
// Polymorphic Lexicon Generation
// ########################################
extern crate skysail;

use env_logger;
use skysail::lexicon::*;
use std::fs::read_dir;
use std::path::Path;

fn main() {
    env_logger::init();
    let root = Path::new("lexicons/");
    if let Err(e) = walk_lexicon(root) {
        println!("Error: {:?}", e);
    };
    println!("Complete.");
}

fn walk_lexicon(dir: &Path) -> Result<(), std::io::Error> {
    if dir.is_dir() {
        for entry in read_dir(dir)? {
            let entry = entry?;
            let path = entry.path();
            if path.is_dir() {
                let _ = walk_lexicon(&path);
            } else if path.extension().unwrap() == "json" {
                let _ = code_generator::lex2rs(&path);
            }
        }
    } else {
        println!("Error: Expected root directory of lexicons.");
    }
    Ok(()) // TODO: Remove silent failure
}
