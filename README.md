# skysail - now compiling to WebAssembly!

This is a Rust library designed for use with the AT Protocol and open social platforms like Bluesky. Although currently in a developmental phase, this library aims to be robust and modular, ensuring ease of development with simple demonstrations ready to go out-of-the-box.

The following milestones will need to be completed before a crate can be released on crates.io

- [ ] Dynamically generate types from Lexicon schema definitions
- [ ] Comprehensive testing, which has not recieved the attention it probably should have
- [ ] Add basic usage examples
- [x] Compile to WASM

![bsky.png](https://i.ibb.co/FXH0ZtJ/image.png)

## Usage
### Run the development server
**Update October 2023:**
*This likely is not the best way to learn about atproto. On the TODO list is adding a "hello-world" example which interacts with the production Big Graph Service (BGS) deployed by Bluesky PBC.*

Clone the atproto repo and checkout the latest branch known to work with this crate:
```
git clone -n https://github.com/bluesky-social/atproto/
cd atproto
git checkout b530a13d09ba8ad4bd600707f94728b14ce62a56
```
### Set up a JavaScript environment:
```
npm install --global yarn
yarn install --frozen-lockfile
yarn build
```
### Start Personal Data Server, and DID:PLC Server
```
cd packages/dev-env
yarn run start
```
Which should drop you at the following prompt:
```
[  v0.0.1  | created by Bluesky ]

Initializing...
[2582] 👤 DID Placeholder server started http://localhost:2582
[2583] 🌞 Personal Data server started http://localhost:2583
(node:676724) ExperimentalWarning: The Fetch API is an experimental feature. This feature could change at any time
(Use `node --trace-warnings ...` to show where the warning was created)
Test environment generated.
Type .help if you get lost
> 

```
## Build and run rust-atp
In a different terminal session, clone and build this repo:
```
git clone https://gitlab.com/jmcasey/skysail
cd skysail
cargo build
```

Unfortunately due to schema changes and a less-than-working code generator, the library is not currently compatible with the live network. Fixing this is my second priority behind completing the code generator. (And this is a personal portfolio projects, no guarantees)

When working, the program creates a new ATP user, receiving a [DID Document](https://www.w3.org/TR/did-core/). Then it will start a session with the PDS, create a new post, and fetch the post to print to the screen. Check out `main.rs` for details on what calls are being made.

## Known Issues
- atproto is under active, daily development. **Things will change.**
- Regular updates to ATP Lexicons will break this library until the code generator is working. Please see the `codegen` branch where effort is currently being focused.
- Not all methods are implemented, most aren't.
- Warnings as far as the eye can see. Mostly for unused variables.
- Hardcoded values: Suitable for testing, but will be prioritised for removal.
- Silliness `with reqwest::Client`:
	- Currently each method call has it's own HTTP request/response code, this will be refactored.
	- Some methods return sensible ATP objects, others return JSON, yet others return raw `reqwest::Responses`.
		- Need to consider what is best, what the developer expects, and what best supports ATP schemas/conventions in the future.
- Crate conventions, efficiencies, naming, etc.
- Should `await`s be exposed to the user of this crate? Unsure on best DX practices.
- ~~Are there a usable DID and CBOR crates available already?~~ Yes! We use them now. :)

